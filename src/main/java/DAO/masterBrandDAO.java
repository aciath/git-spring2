/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.masterBrandMod;
//import com.zaxxer.hikari.HikariConfig;
//import com.zaxxer.hikari.HikariDataSource;
import dbConnection.dbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class masterBrandDAO {
    private Connection conn = null;
    
//    HikariDataSource hikariDataSource;
//    HikariConfig hikariConfig;
//    
//    HikariConfig config = new HikariConfig("hikari.properties");
//    HikariDataSource ds = new HikariDataSource(config);
    
    public String getAllmasterBrand(){
        String jsonResponse = "";
        
        String brandId = "";
        String brandName = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_BRAND = new JSONArray();
 
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            String query = "SELECT brand_id, brand_name FROM m_brand";
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                brandId = rs.getString("BRAND_ID");
                brandName = rs.getString("BRAND_NAME");
                
                JSONObject DATA_BRAND = new JSONObject();
                
                DATA_BRAND.put("BRAND_ID", new String(brandId));
                DATA_BRAND.put("BRAND_NAME", new String(brandName));
                
                DATA_MASTER_BRAND.put(DATA_BRAND);
            }
            
            JSONObjectRoot.put("DATA_MASTER_BRAND", DATA_MASTER_BRAND);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String getMasterBrand(masterBrandMod MBM){
        String jsonResponse = "";
        
        String brandId = "";
        String brandName = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_BRAND = new JSONArray();
 
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("SELECT brand_id, brand_name FROM m_brand WHERE brand_id = ?");
            ps.setString(1, MBM.getBrandId());
            rs = ps.executeQuery();
            
            while (rs.next()) {
                brandId = rs.getString("BRAND_ID");
                brandName = rs.getString("BRAND_NAME");
                
                JSONObject DATA_BRAND = new JSONObject();
                
                DATA_BRAND.put("BRAND_ID", new String(brandId));
                DATA_BRAND.put("BRAND_NAME", new String(brandName));
                
                DATA_MASTER_BRAND.put(DATA_BRAND);
            }
            
            JSONObjectRoot.put("DATA_MASTER_BRAND", DATA_MASTER_BRAND);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String saveMasterBrand(masterBrandMod MBM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_BRAND = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("INSERT INTO m_brand VALUES (?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, MBM.getBrandId());
            ps.setString(2, MBM.getBrandName());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success add brand.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_BRAND = new JSONObject();
        
        DATA_BRAND.put("RESULT", new Boolean(result));
        DATA_BRAND.put("MESSAGE", new String(messageResult));
        DATA_MASTER_BRAND.put(DATA_BRAND);
            
        JSONObjectRoot.put("DATA_MASTER_BRAND", DATA_MASTER_BRAND);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String updateMasterBrand(masterBrandMod MBM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_BRAND = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("UPDATE m_brand SET brand_name = ? WHERE brand_id = ?", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, MBM.getBrandName());
            ps.setString(2, MBM.getBrandId());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success update brand.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_BRAND = new JSONObject();
        
        DATA_BRAND.put("RESULT", new Boolean(result));
        DATA_BRAND.put("MESSAGE", new String(messageResult));
        DATA_MASTER_BRAND.put(DATA_BRAND);
            
        JSONObjectRoot.put("DATA_MASTER_BRAND", DATA_MASTER_BRAND);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String deleteMasterBrand(masterBrandMod MBM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_BRAND = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("DELETE FROM m_brand WHERE brand_id = ?", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, MBM.getBrandId());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success delete brand.";
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_BRAND = new JSONObject();
        
        DATA_BRAND.put("RESULT", new Boolean(result));
        DATA_BRAND.put("MESSAGE", new String(messageResult));
        DATA_MASTER_BRAND.put(DATA_BRAND);
            
        JSONObjectRoot.put("DATA_MASTER_BRAND", DATA_MASTER_BRAND);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
}
