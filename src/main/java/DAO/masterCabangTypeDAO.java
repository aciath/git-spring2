/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.masterCabangTypeMod;
import dbConnection.dbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class masterCabangTypeDAO {
    private Connection conn = null;
    
    public String getAllMasterCabangType(){
        String jsonResponse = "";
    
        String typeId = "";
        String typeName = "";
        String typeBu = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_CABANG_TYPE = new JSONArray();
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            String query = "SELECT type_id, type_name, type_bu FROM m_cabang_type";
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                typeId = rs.getString("TYPE_ID");
                typeName = rs.getString("TYPE_NAME");
                typeBu = rs.getString("TYPE_BU");
                
                JSONObject DATA_CABANG_TYPE = new JSONObject();
                
                DATA_CABANG_TYPE.put("TYPE_ID", new String(typeId));
                DATA_CABANG_TYPE.put("TYPE_NAME", new String(typeName));
                DATA_CABANG_TYPE.put("TYPE_BU", new String(typeBu));
                
                DATA_MASTER_CABANG_TYPE.put(DATA_CABANG_TYPE);
            }
            
            JSONObjectRoot.put("DATA_MASTER_CABANG_TYPE", DATA_MASTER_CABANG_TYPE);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String getAllMasterBusinessUnit(){
        String jsonResponse = "";
    
        String typeBu = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_CABANG_TYPE = new JSONArray();
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            String query = "SELECT DISTINCT type_bu FROM m_cabang_type";
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                typeBu = rs.getString("TYPE_BU");
                
                JSONObject DATA_CABANG_TYPE = new JSONObject();
                
                DATA_CABANG_TYPE.put("TYPE_BU", new String(typeBu));
                
                DATA_MASTER_CABANG_TYPE.put(DATA_CABANG_TYPE);
            }
            
            JSONObjectRoot.put("DATA_MASTER_CABANG_TYPE", DATA_MASTER_CABANG_TYPE);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String getMasterCabangType(masterCabangTypeMod MCTM){
        String jsonResponse = "";
        
        String typeId = "";
        String typeName = "";
        String typeBu = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_CABANG_TYPE = new JSONArray();
 
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("SELECT type_id, type_name, type_bu FROM m_cabang_type WHERE type_id = ?");
            ps.setString(1, MCTM.getTypeId());
            rs = ps.executeQuery();
            
            while (rs.next()) {
                typeId = rs.getString("TYPE_ID");
                typeName = rs.getString("TYPE_NAME");
                typeBu = rs.getString("TYPE_BU");
                
                JSONObject DATA_CABANG_TYPE = new JSONObject();
                
                DATA_CABANG_TYPE.put("TYPE_ID", new String(typeId));
                DATA_CABANG_TYPE.put("TYPE_NAME", new String(typeName));
                DATA_CABANG_TYPE.put("TYPE_BU", new String(typeBu));
                
                DATA_MASTER_CABANG_TYPE.put(DATA_CABANG_TYPE);
            }
            
            JSONObjectRoot.put("DATA_MASTER_CABANG_TYPE", DATA_MASTER_CABANG_TYPE);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String saveMasterCabangType(masterCabangTypeMod MCTM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_CABANG_TYPE = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("INSERT INTO m_cabang_type VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, MCTM.getTypeId());
            ps.setString(2, MCTM.getTypeName());
            ps.setString(3, MCTM.getTypeBu());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success add cabang type.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_CABANG_TYPE = new JSONObject();
        
        DATA_CABANG_TYPE.put("RESULT", new Boolean(result));
        DATA_CABANG_TYPE.put("MESSAGE", new String(messageResult));
        DATA_MASTER_CABANG_TYPE.put(DATA_CABANG_TYPE);
            
        JSONObjectRoot.put("DATA_MASTER_CABANG_TYPE", DATA_MASTER_CABANG_TYPE);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String updateMasterCabangType(masterCabangTypeMod MCTM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_CABANG_TYPE = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("UPDATE m_cabang_type SET type_name = ?, type_bu = ? WHERE type_id = ?"); 
            ps.setString(1, MCTM.getTypeName());
            ps.setString(2, MCTM.getTypeBu());
            ps.setString(3, MCTM.getTypeId());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success edit cabang type.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_CABANG_TYPE = new JSONObject();
        
        DATA_CABANG_TYPE.put("RESULT", new Boolean(result));
        DATA_CABANG_TYPE.put("MESSAGE", new String(messageResult));
        DATA_MASTER_CABANG_TYPE.put(DATA_CABANG_TYPE);
            
        JSONObjectRoot.put("DATA_MASTER_CABANG_TYPE", DATA_MASTER_CABANG_TYPE);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String deleteMasterCabangType(masterCabangTypeMod MCTM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_CABANG_TYPE = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("DELETE FROM m_cabang_type WHERE type_id = ?"); 
            ps.setString(1, MCTM.getTypeId());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success delete cabang type.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_CABANG_TYPE = new JSONObject();
        
        DATA_CABANG_TYPE.put("RESULT", new Boolean(result));
        DATA_CABANG_TYPE.put("MESSAGE", new String(messageResult));
        DATA_MASTER_CABANG_TYPE.put(DATA_CABANG_TYPE);
            
        JSONObjectRoot.put("DATA_MASTER_CABANG_TYPE", DATA_MASTER_CABANG_TYPE);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
}
