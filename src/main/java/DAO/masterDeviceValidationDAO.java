/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.masterDeviceValidationMod;
import dbConnection.dbConnection;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 *
 * @author user
 */
public class masterDeviceValidationDAO {
    private Connection conn = null;
    
    public String getAllDeviceValidation(){
        String jsonResponse = "";
    
        String recid = "";
        String deviceImei = "";
        String deviceVersionRel = "";
        String deviceVersionSdk = "";
        String deviceBoard = "";
        String deviceBrand = "";
        String deviceId = "";
        String deviceManufacturer = "";
        String deviceModel = "";
        String deviceProduct = "";
        String deviceSerialNumber = "";
        String deviceTime = "";
        String deviceType = "";
        String deviceUser = "";
        String deviceGpsLatitude = "";
        String deviceGpsLongitude = "";
        String deviceVerificationKey = "";
        String deviceVerificationDate = "";
        String isactive = "";
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_DEVICE_VALIDATION = new JSONArray();
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            String query = "SELECT recid, device_imei, device_version_rel, device_version_sdk, device_board, device_brand, device_id, device_manufacturer, device_model, device_product, device_serial_number, device_time, device_type, device_user, device_gps_latitude, device_gps_longitude, device_verification_key, device_verification_date, isactive, created_opt, created_dt, edited_opt, edited_dt, client_addr FROM m_device_validation";
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                recid = (rs.getString("RECID") != null) ? rs.getString("RECID") : "";
                deviceImei = (rs.getString("DEVICE_IMEI") != null) ? rs.getString("DEVICE_IMEI") : "";
                deviceVersionRel = (rs.getString("DEVICE_VERSION_REL") != null) ? rs.getString("DEVICE_VERSION_REL") : "";
                deviceVersionSdk = (rs.getString("DEVICE_VERSION_SDK") != null) ? rs.getString("DEVICE_VERSION_SDK") : "";
                deviceBoard = (rs.getString("DEVICE_BOARD") != null) ? rs.getString("DEVICE_BOARD") : "";
                deviceBrand = (rs.getString("DEVICE_BRAND") != null) ? rs.getString("DEVICE_BRAND") : "";
                deviceId = (rs.getString("DEVICE_ID") != null) ? rs.getString("DEVICE_ID") : "";
                deviceManufacturer = (rs.getString("DEVICE_MANUFACTURER") != null) ? rs.getString("DEVICE_MANUFACTURER") : "";
                deviceModel = (rs.getString("DEVICE_MODEL") != null) ? rs.getString("DEVICE_MODEL") : "";
                deviceProduct = (rs.getString("DEVICE_PRODUCT") != null) ? rs.getString("DEVICE_PRODUCT") : "";
                deviceSerialNumber = (rs.getString("DEVICE_SERIAL_NUMBER") != null) ? rs.getString("DEVICE_SERIAL_NUMBER") : "";
                deviceTime = (rs.getString("DEVICE_TIME") != null) ? rs.getString("DEVICE_TIME") : "";
                deviceType = (rs.getString("DEVICE_TYPE") != null) ? rs.getString("DEVICE_TYPE") : "";
                deviceUser = (rs.getString("DEVICE_USER") != null) ? rs.getString("DEVICE_USER") : "";
                deviceGpsLatitude = (rs.getString("DEVICE_GPS_LATITUDE") != null) ? rs.getString("DEVICE_GPS_LATITUDE") : "";
                deviceGpsLongitude = (rs.getString("DEVICE_GPS_LONGITUDE") != null) ? rs.getString("DEVICE_GPS_LONGITUDE") : "";
                deviceVerificationKey = (rs.getString("DEVICE_VERIFICATION_KEY") != null) ? rs.getString("DEVICE_VERIFICATION_KEY") : "";
                deviceVerificationDate = (rs.getString("DEVICE_VERIFICATION_DATE") != null) ? rs.getString("DEVICE_VERIFICATION_DATE") : "";
                isactive = (rs.getString("ISACTIVE") != null) ? rs.getString("ISACTIVE") : "";
                createdOpt = (rs.getString("CREATED_OPT") != null) ? rs.getString("CREATED_OPT") : "";
                createdDt = (rs.getString("CREATED_DT") != null) ? rs.getString("CREATED_DT") : "";
                editedOpt = (rs.getString("EDITED_OPT") != null) ? rs.getString("EDITED_OPT") : "";
                editedDt = (rs.getString("EDITED_DT") != null) ? rs.getString("EDITED_DT") : "";
                clientAddr = (rs.getString("CLIENT_ADDR") != null) ? rs.getString("CLIENT_ADDR") : "";

                JSONObject DATA_DEVICE_VALIDATION = new JSONObject();
                
                DATA_DEVICE_VALIDATION.put("RECID", new String(recid));
                DATA_DEVICE_VALIDATION.put("DEVICE_IMEI", new String(deviceImei));
                DATA_DEVICE_VALIDATION.put("DEVICE_VERSION_REL", new String(deviceVersionRel));
                DATA_DEVICE_VALIDATION.put("DEVICE_VERSION_SDK", new String(deviceVersionSdk));
                DATA_DEVICE_VALIDATION.put("DEVICE_BOARD", new String(deviceBoard));
                DATA_DEVICE_VALIDATION.put("DEVICE_BRAND", new String(deviceBrand));
                DATA_DEVICE_VALIDATION.put("DEVICE_ID", new String(deviceId));
                DATA_DEVICE_VALIDATION.put("DEVICE_MANUFACTURER", new String(deviceManufacturer));
                DATA_DEVICE_VALIDATION.put("DEVICE_MODEL", new String(deviceModel));
                DATA_DEVICE_VALIDATION.put("DEVICE_PRODUCT", new String(deviceProduct));
                DATA_DEVICE_VALIDATION.put("DEVICE_SERIAL_NUMBER", new String(deviceSerialNumber));
                DATA_DEVICE_VALIDATION.put("DEVICE_TIME", new String(deviceTime));
                DATA_DEVICE_VALIDATION.put("DEVICE_TYPE", new String(deviceType));
                DATA_DEVICE_VALIDATION.put("DEVICE_USER", new String(deviceUser));
                DATA_DEVICE_VALIDATION.put("DEVICE_GPS_LATITUDE", new String(deviceGpsLatitude));
                DATA_DEVICE_VALIDATION.put("DEVICE_GPS_LONGITUDE", new String(deviceGpsLongitude));
                DATA_DEVICE_VALIDATION.put("DEVICE_VERIFICATION_KEY", new String(deviceVerificationKey));
                DATA_DEVICE_VALIDATION.put("DEVICE_VERIFICATION_DATE", new String(deviceVerificationDate));
                DATA_DEVICE_VALIDATION.put("ISACTIVE", new String(isactive));
                DATA_DEVICE_VALIDATION.put("CREATED_OPT", new String(createdOpt));
                DATA_DEVICE_VALIDATION.put("CREATED_DT", new String(createdDt));
                DATA_DEVICE_VALIDATION.put("EDITED_OPT", new String(editedOpt));
                DATA_DEVICE_VALIDATION.put("EDITED_DT", new String(editedDt));
                DATA_DEVICE_VALIDATION.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_DEVICE_VALIDATION.put(DATA_DEVICE_VALIDATION);
            }
            
            JSONObjectRoot.put("DATA_MASTER_DEVICE_VALIDATION", DATA_MASTER_DEVICE_VALIDATION);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String getDeviceValidation(masterDeviceValidationMod MDVM){
        String jsonResponse = "";
        
        String recid = "";
        String deviceImei = "";
        String deviceVersionRel = "";
        String deviceVersionSdk = "";
        String deviceBoard = "";
        String deviceBrand = "";
        String deviceId = "";
        String deviceManufacturer = "";
        String deviceModel = "";
        String deviceProduct = "";
        String deviceSerialNumber = "";
        String deviceTime = "";
        String deviceType = "";
        String deviceUser = "";
        String deviceGpsLatitude = "";
        String deviceGpsLongitude = "";
        String deviceVerificationKey = "";
        String deviceVerificationDate = "";
        String isactive = "";
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_DEVICE_VALIDATION = new JSONArray();
 
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("SELECT recid, device_imei, device_version_rel, device_version_sdk, device_board, device_brand, device_id, device_manufacturer, device_model, device_product, device_serial_number, device_time, device_type, device_user, device_gps_latitude, device_gps_longitude, device_verification_key, device_verification_date, isactive, created_opt, created_dt, edited_opt, edited_dt, client_addr FROM m_device_validation WHERE recid = ?");
            ps.setString(1, String.valueOf(MDVM.getRecid()));
            rs = ps.executeQuery();
            
            while (rs.next()) {
                recid = rs.getString("RECID");
                deviceImei = rs.getString("DEVICE_IMEI");
                deviceVersionRel = rs.getString("DEVICE_VERSION_REL");
                deviceVersionSdk = rs.getString("DEVICE_VERSION_SDK");
                deviceBoard = rs.getString("DEVICE_BOARD");
                deviceBrand = rs.getString("DEVICE_BRAND");
                deviceId = rs.getString("DEVICE_ID");
                deviceManufacturer = rs.getString("DEVICE_MANUFACTURER");
                deviceModel = rs.getString("DEVICE_MODEL");
                deviceProduct = rs.getString("DEVICE_PRODUCT");
                deviceSerialNumber = rs.getString("DEVICE_SERIAL_NUMBER");
                deviceTime = rs.getString("DEVICE_TIME");
                deviceType = rs.getString("DEVICE_TYPE");
                deviceUser = rs.getString("DEVICE_USER");
                deviceGpsLatitude = rs.getString("DEVICE_GPS_LATITUDE");
                deviceGpsLongitude = rs.getString("DEVICE_GPS_LONGITUDE");
                deviceVerificationKey = rs.getString("DEVICE_VERIFICATION_KEY");
                deviceVerificationDate = rs.getString("DEVICE_VERIFICATION_DATE");
                isactive = rs.getString("ISACTIVE");
                createdOpt = rs.getString("CREATED_OPT");
                createdDt = rs.getString("CREATED_DT");
                editedOpt = rs.getString("EDITED_OPT");
                editedDt = rs.getString("EDITED_DT");
                clientAddr = rs.getString("CLIENT_ADDR");

                JSONObject DATA_DEVICE_VALIDATION = new JSONObject();
                
                DATA_DEVICE_VALIDATION.put("RECID", new String(recid));
                DATA_DEVICE_VALIDATION.put("DEVICE_IMEI", new String(deviceImei));
                DATA_DEVICE_VALIDATION.put("DEVICE_VERSION_REL", new String(deviceVersionRel));
                DATA_DEVICE_VALIDATION.put("DEVICE_VERSION_SDK", new String(deviceVersionSdk));
                DATA_DEVICE_VALIDATION.put("DEVICE_BOARD", new String(deviceBoard));
                DATA_DEVICE_VALIDATION.put("DEVICE_BRAND", new String(deviceBrand));
                DATA_DEVICE_VALIDATION.put("DEVICE_ID", new String(deviceId));
                DATA_DEVICE_VALIDATION.put("DEVICE_MANUFACTURER", new String(deviceManufacturer));
                DATA_DEVICE_VALIDATION.put("DEVICE_MODEL", new String(deviceModel));
                DATA_DEVICE_VALIDATION.put("DEVICE_PRODUCT", new String(deviceProduct));
                DATA_DEVICE_VALIDATION.put("DEVICE_SERIAL_NUMBER", new String(deviceSerialNumber));
                DATA_DEVICE_VALIDATION.put("DEVICE_TIME", new String(deviceTime));
                DATA_DEVICE_VALIDATION.put("DEVICE_TYPE", new String(deviceType));
                DATA_DEVICE_VALIDATION.put("DEVICE_USER", new String(deviceUser));
                DATA_DEVICE_VALIDATION.put("DEVICE_GPS_LATITUDE", new String(deviceGpsLatitude));
                DATA_DEVICE_VALIDATION.put("DEVICE_GPS_LONGITUDE", new String(deviceGpsLongitude));
                DATA_DEVICE_VALIDATION.put("ISACTIVE", new String(isactive));
                DATA_DEVICE_VALIDATION.put("CREATED_OPT", new String(createdOpt));
                DATA_DEVICE_VALIDATION.put("CREATED_DT", new String(createdDt));
                DATA_DEVICE_VALIDATION.put("EDITED_OPT", new String(editedOpt));
                DATA_DEVICE_VALIDATION.put("EDITED_DT", new String(editedDt));
                DATA_DEVICE_VALIDATION.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_DEVICE_VALIDATION.put(DATA_DEVICE_VALIDATION);
            }
            
            JSONObjectRoot.put("DATA_MASTER_DEVICE_VALIDATION", DATA_MASTER_DEVICE_VALIDATION);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String saveMasterDeviceValidation(masterDeviceValidationMod MDVM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_DEVICE_VALIDATION = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("INSERT INTO m_device_validation (device_imei, device_version_rel, device_version_sdk, device_board, device_brand, device_id, device_manufacturer, device_model, device_product, device_serial_number, device_time, device_type, device_user, device_gps_latitude, device_gps_longitude, device_verification_key, device_verification_date, isactive, created_opt) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
//            ps.setString(1, MDVM.getDeviceImei());
//            ps.setString(2, MDVM.getDeviceVersionRel());
//            ps.setString(3, MDVM.getDeviceVersionSdk());
//            ps.setString(4, MDVM.getDeviceBoard());
//            ps.setString(5, MDVM.getDeviceBrand());
//            ps.setString(6, MDVM.getDeviceId());
//            ps.setString(7, MDVM.getDeviceManufacturer());
//            ps.setString(8, MDVM.getDeviceModel());
//            ps.setString(9, MDVM.getDeviceProduct());
//            ps.setString(10, MDVM.getDeviceSerialNumber());
//            ps.setString(11, MDVM.getDeviceTime());
//            ps.setString(12, MDVM.getDeviceType());
//            ps.setString(13, MDVM.getDeviceUser());
//            ps.setString(14, MDVM.getDeviceGPSLatitude());
//            ps.setString(15, MDVM.getDeviceGPSLongitude());
//            ps.setString(16, MDVM.getDeviceVerificationKey());
//            ps.setString(17, MDVM.getDeviceVerificationDate());
//            ps.setString(18, MDVM.getIsActive());
//            ps.setString(19, MDVM.getCreatedOpt());
//            ps.setString(20, MDVM.getCreatedDT());
//            ps.setString(21, MDVM.getEditedOpt());
//            ps.setString(22, MDVM.getEditedDT());
//            ps.setString(23, MDVM.getClientAddr());
            
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
            String stringDate = ft.format(dNow);
      
            ps.setString(1, MDVM.getDeviceImei());
            ps.setString(2, "");
            ps.setString(3, "");
            ps.setString(4, "");
            ps.setString(5, "");
            ps.setString(6, "");
            ps.setString(7, "");
            ps.setString(8, MDVM.getDeviceModel());
            ps.setString(9, MDVM.getDeviceProduct());
            ps.setString(10, MDVM.getDeviceSerialNumber());
            ps.setString(11, "");
            ps.setString(12, "");
            ps.setString(13, "");
            ps.setString(14, "");
            ps.setString(15, "");
            ps.setString(16, "");
            ps.setString(17, "2001-01-01 00:00:00"); // datetime
            ps.setString(18, "");
            ps.setString(19, MDVM.getCreatedOpt());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success add device to the server.";
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_DEVICE_VALIDATION = new JSONObject();
        
        DATA_DEVICE_VALIDATION.put("RESULT", new Boolean(result));
        DATA_DEVICE_VALIDATION.put("MESSAGE", new String(messageResult));
        DATA_MASTER_DEVICE_VALIDATION.put(DATA_DEVICE_VALIDATION);
            
        JSONObjectRoot.put("DATA_MASTER_DEVICE_VALIDATION", DATA_MASTER_DEVICE_VALIDATION);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String updateMasterDeviceValidation(masterDeviceValidationMod MDVM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_DEVICE_VALIDATION = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
        String stringDate = ft.format(dNow);
            
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
            
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("UPDATE m_device_validation SET "
                    + "device_imei = ?, "
//                    + "device_version_rel = ?, "
//                    + "device_version_sdk = ?, "
//                    + "device_board = ?, "
//                    + "device_brand = ?, "
//                    + "device_id = ?, "
//                    + "device_manufacturer = ?, "
                    + "device_model = ?, "
                    + "device_product = ?, "
                    + "device_serial_number = ?, "
//                    + "device_time = ?, "
//                    + "device_type = ?, "
//                    + "device_user = ?, "
//                    + "device_gps_latitude = ?, "
//                    + "device_gps_longitude = ?, "
                    + "device_verification_key = ?, "
                    + "device_verification_date = ?, "
//                    + "isactive = ?, "
//                    + "created_opt = ?, "
//                    + "created_dt = ?, "
                    + "edited_opt = ?, "
                    + "edited_dt = ? "
//                    + "client_addr = ? "
                    + "WHERE recid = ?"); 
            ps.setString(1, MDVM.getDeviceImei());
//            ps.setString(2, MDVM.getDeviceVersionRel());
//            ps.setString(3, MDVM.getDeviceVersionSdk());
//            ps.setString(4, MDVM.getDeviceBoard());
//            ps.setString(5, MDVM.getDeviceBrand());
//            ps.setString(6, MDVM.getDeviceId());
//            ps.setString(7, MDVM.getDeviceManufacturer());
            ps.setString(2, MDVM.getDeviceModel());
            ps.setString(3, MDVM.getDeviceProduct());
            ps.setString(4, MDVM.getDeviceSerialNumber());
//            ps.setString(11, MDVM.getDeviceTime());
//            ps.setString(12, MDVM.getDeviceType());
//            ps.setString(13, MDVM.getDeviceUser());
//            ps.setString(14, MDVM.getDeviceGPSLatitude());
//            ps.setString(15, MDVM.getDeviceGPSLongitude());
            ps.setString(5, "");
            ps.setString(6, "2001-01-01 00:00:00");
//            ps.setString(18, MDVM.getIsActive());
//            ps.setString(19, MDVM.getCreatedOpt());
//            ps.setString(20, MDVM.getCreatedDT());
            ps.setString(7, MDVM.getEditedOpt());
            ps.setString(8, stringDate);
//            ps.setString(23, MDVM.getClientAddr());
            ps.setString(9, String.valueOf(MDVM.getRecid()));
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success edit device validation data.";
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
            messageResult = stringDate+" - "+e.getMessage();
        }
        
        JSONObject DATA_DEVICE_VALIDATION = new JSONObject();
        
        DATA_DEVICE_VALIDATION.put("RESULT", new Boolean(result));
        DATA_DEVICE_VALIDATION.put("MESSAGE", new String(messageResult));
        DATA_MASTER_DEVICE_VALIDATION.put(DATA_DEVICE_VALIDATION);
            
        JSONObjectRoot.put("DATA_MASTER_DEVICE_VALIDATION", DATA_MASTER_DEVICE_VALIDATION);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String deleteMasterDeviceValidation(masterDeviceValidationMod MDVM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_DEVICE_VALIDATION = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("DELETE FROM m_device_validation WHERE device_imei = ? AND device_serial_number = ?"); 
            ps.setString(1, MDVM.getDeviceImei());
            ps.setString(2, MDVM.getDeviceSerialNumber());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success delete device.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_DEVICE_VALIDATION = new JSONObject();
        
        DATA_DEVICE_VALIDATION.put("RESULT", new Boolean(result));
        DATA_DEVICE_VALIDATION.put("MESSAGE", new String(messageResult));
        DATA_MASTER_DEVICE_VALIDATION.put(DATA_DEVICE_VALIDATION);
            
        JSONObjectRoot.put("DATA_MASTER_DEVICE_VALIDATION", DATA_MASTER_DEVICE_VALIDATION);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String validatingDevice(masterDeviceValidationMod MDVM) throws UnsupportedEncodingException, JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_DEVICE_VALIDATION = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
        
            ResultSet rs;
            
            String encryptedMessage = "";
            String validateMessage = "";
            String statusLog = "";
            String dbIMEI = "";
            String validationNumber = "";
            
            java.util.Date utilDate = new java.util.Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(utilDate);
            cal.set(Calendar.MILLISECOND, 0);
            java.sql.Timestamp serverTimestamp = new java.sql.Timestamp(cal.getTimeInMillis());
             
            stmt = conn.createStatement();
            String query = "SELECT recid, device_imei, device_version_rel, device_version_sdk, device_board, device_brand, device_id, device_manufacturer, device_model, device_product, device_serial_number, device_time, device_type, device_user, device_gps_latitude, device_gps_longitude, device_verification_key, device_verification_date, isactive, created_opt, created_dt, edited_opt, edited_dt, client_addr FROM m_device_validation WHERE device_imei = '"+MDVM.getDeviceImei()+"'";
            rs = stmt.executeQuery(query);
            
            while(rs.next()){
                dbIMEI = rs.getString("DEVICE_IMEI");
                validationNumber = rs.getString("DEVICE_VERIFICATION_KEY");
                if(dbIMEI.equals(MDVM.getDeviceImei()) && validationNumber.equals("")){
                    byte[] messageByte = MDVM.getDeviceImei().getBytes("UTF-8");
                    MessageDigest MD = MessageDigest.getInstance("MD5");
                    byte[] digestiv = MD.digest(messageByte);
                    BigInteger bi = new BigInteger(1, digestiv);
                    encryptedMessage = String.format("%0" + (digestiv.length << 1) + "X", bi);
                                            
                    PreparedStatement ps2 = this.conn.prepareStatement("UPDATE WTDP_DEVICE SET "
                                + "DEVICE_VERSION_REL = ?, "
                                + "DEVICE_VERSION_SDK = ?, "
                                + "DEVICE_BOARD = ?, "
                                + "DEVICE_BRAND = ?, "
                                + "DEVICE_ID = ?, "
                                + "DEVICE_MANUFACTURER = ?, "
                                + "DEVICE_MODEL = ?, "
                                + "DEVICE_PRODUCT = ?, "
                                + "DEVICE_SERIAL_NUMBER = ?, "
                                + "DEVICE_TIME = ?, "
                                + "DEVICE_TYPE = ?, "
                                + "DEVICE_USER = ?, "
                                + "DEVICE_VERIFICATION_KEY = ?, "
                                + "DEVICE_VERIFICATION_DATE = ? "
                                + "WHERE DEVICE_IMEI = ?");
                        ps2.setString(1, MDVM.getDeviceVersionRel());
                        ps2.setString(2, MDVM.getDeviceVersionSdk());
                        ps2.setString(3, MDVM.getDeviceBoard());
                        ps2.setString(4, MDVM.getDeviceBrand());
                        ps2.setString(5, MDVM.getDeviceId());
                        ps2.setString(6, MDVM.getDeviceManufacturer());
                        ps2.setString(7, MDVM.getDeviceModel());
                        ps2.setString(8, MDVM.getDeviceProduct());
                        ps2.setString(9, MDVM.getDeviceSerialNumber());
                        ps2.setString(10, MDVM.getDeviceTime());
                        ps2.setString(11, MDVM.getDeviceType());
                        ps2.setString(12, MDVM.getDeviceUser());
                        ps2.setString(13, encryptedMessage);
                        ps2.setTimestamp(14, serverTimestamp);
                        ps2.setString(15, MDVM.getDeviceImei());
                    
                    if(ps2.executeUpdate() > 0){
                        result = true;
                        messageResult = "Success validating device.";
                    } else {
                        result = false;
                        messageResult = "Validating device failed.";
                    }
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_DEVICE_VALIDATION = new JSONObject();
        
        DATA_DEVICE_VALIDATION.put("RESULT", new Boolean(result));
        DATA_DEVICE_VALIDATION.put("MESSAGE", new String(messageResult));
        DATA_MASTER_DEVICE_VALIDATION.put(DATA_DEVICE_VALIDATION);
            
        JSONObjectRoot.put("DATA_MASTER_DEVICE_VALIDATION", DATA_MASTER_DEVICE_VALIDATION);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }   
}
