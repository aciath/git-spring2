/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.masterWalkthroughActivitiesMod;
import dbConnection.dbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class masterWalkthroughActivitiesDAO {
    private Connection conn = null;
    
    public String getAllMasterWalkthroughActivities(){
        String jsonResponse = "";
    
        String activitiesBU = "";
        String activitiesDiv = "";
        String activitiesName = "";
        String activitiesTimeStart = "";
        String activitiesTimeEnd = "";
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_ACTIVITIES = new JSONArray();
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            String query = "SELECT activities_bu, activities_div, activities_name, activities_time_start, activities_time_end, created_opt, created_dt, edited_opt, edited_dt, client_addr FROM m_wtr_activities";
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                activitiesBU = (rs.getString("ACTIVITIES_BU") != null) ? rs.getString("ACTIVITIES_BU") : "";
                activitiesDiv = (rs.getString("ACTIVITIES_DIV") != null) ? rs.getString("ACTIVITIES_DIV") : "";
                activitiesName = (rs.getString("ACTIVITIES_NAME") != null) ? rs.getString("ACTIVITIES_NAME") : "";
                activitiesTimeStart = (rs.getString("ACTIVITIES_TIME_START") != null) ? rs.getString("ACTIVITIES_TIME_START") : "";
                activitiesTimeEnd = (rs.getString("ACTIVITIES_TIME_END") != null) ? rs.getString("ACTIVITIES_TIME_END") : "";
                createdOpt = (rs.getString("CREATED_OPT") != null) ? rs.getString("CREATED_OPT") : "";
                createdDt = (rs.getString("CREATED_DT") != null) ? rs.getString("CREATED_DT") : "";
                editedOpt = (rs.getString("EDITED_OPT") != null) ? rs.getString("EDITED_OPT") : "";
                editedDt = (rs.getString("EDITED_DT") != null) ? rs.getString("EDITED_DT") : "";
                clientAddr = (rs.getString("CLIENT_ADDR") != null) ? rs.getString("CLIENT_ADDR") : "";
                
                JSONObject DATA_WALKTHROUGH_ACTIVITIES = new JSONObject();
                
                DATA_WALKTHROUGH_ACTIVITIES.put("ACTIVITIES_BU", new String(activitiesBU));
                DATA_WALKTHROUGH_ACTIVITIES.put("ACTIVITIES_DIV", new String(activitiesDiv));
                DATA_WALKTHROUGH_ACTIVITIES.put("ACTIVITIES_NAME", new String(activitiesName));
                DATA_WALKTHROUGH_ACTIVITIES.put("ACTIVITIES_TIME_START", new String(activitiesTimeStart));
                DATA_WALKTHROUGH_ACTIVITIES.put("ACTIVITIES_TIME_END", new String(activitiesTimeEnd));
                DATA_WALKTHROUGH_ACTIVITIES.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_ACTIVITIES.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_ACTIVITIES.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_ACTIVITIES.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_ACTIVITIES.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_WALKTHROUGH_ACTIVITIES.put(DATA_WALKTHROUGH_ACTIVITIES);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_ACTIVITIES", DATA_MASTER_WALKTHROUGH_ACTIVITIES);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String getMasterWalkthroughActivities(masterWalkthroughActivitiesMod MWAM){
        String jsonResponse = "";
        
        String activitiesBU = "";
        String activitiesDiv = "";
        String activitiesName = "";
        String activitiesTimeStart = "";
        String activitiesTimeEnd = "";
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_ACTIVITIES = new JSONArray();
 
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("SELECT activities_bu, activities_div, activities_name, activities_time_start, activities_time_end, created_opt, created_dt, edited_opt, edited_bu, client_addr FROM m_wtr_activities WHERE activities_bu = ?");
            ps.setString(1, MWAM.getActivitiesBu());
            rs = ps.executeQuery();
            
            while (rs.next()) {
                activitiesBU = rs.getString("ACTIVITIES_BU");
                activitiesDiv = rs.getString("ACTIVITIES_DIV");
                activitiesName = rs.getString("ACTIVITIES_NAME");
                activitiesTimeStart = rs.getString("ACTIVITIES_TIME_START");
                activitiesTimeEnd = rs.getString("ACTIVITIES_TIME_END");
                createdOpt = rs.getString("CREATED_OPT");
                createdDt = rs.getString("CREATED_DT");
                editedOpt = rs.getString("EDITED_OPT");
                editedDt = rs.getString("EDITED_DT");
                clientAddr = rs.getString("CLIENT_ADDR");
                
                JSONObject DATA_WALKTHROUGH_ACTIVITIES = new JSONObject();
                
                DATA_WALKTHROUGH_ACTIVITIES.put("ACTIVITIES_BU", new String(activitiesBU));
                DATA_WALKTHROUGH_ACTIVITIES.put("ACTIVITIES_DIV", new String(activitiesDiv));
                DATA_WALKTHROUGH_ACTIVITIES.put("ACTIVITIES_NAME", new String(activitiesName));
                DATA_WALKTHROUGH_ACTIVITIES.put("ACTIVITIES_TIME_START", new String(activitiesTimeStart));
                DATA_WALKTHROUGH_ACTIVITIES.put("ACTIVITIES_TIME_END", new String(activitiesTimeEnd));
                DATA_WALKTHROUGH_ACTIVITIES.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_ACTIVITIES.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_ACTIVITIES.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_ACTIVITIES.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_ACTIVITIES.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_WALKTHROUGH_ACTIVITIES.put(DATA_WALKTHROUGH_ACTIVITIES);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_ACTIVITIES", DATA_MASTER_WALKTHROUGH_ACTIVITIES);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String saveMasterWalkthroughActivities(masterWalkthroughActivitiesMod MWAM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_ACTIVITIES = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("INSERT INTO m_wtr_activities (activities_bu, activities_div, activities_name, activities_time_start, activities_time_end, created_opt) VALUES (?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, MWAM.getActivitiesBu());
            ps.setString(2, MWAM.getActivitiesDiv());
            ps.setString(3, MWAM.getActivitiesName());
            ps.setString(4, MWAM.getActivitiesTimeStart()+":00");
            ps.setString(5, MWAM.getActivitiesTimeEnd()+":00");
            ps.setString(6, MWAM.getCreatedOpt());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success add walkthrough activities.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_ACTIVITIES = new JSONObject();
        
        DATA_WALKTHROUGH_ACTIVITIES.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_ACTIVITIES.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_ACTIVITIES.put(DATA_WALKTHROUGH_ACTIVITIES);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_ACTIVITIES", DATA_MASTER_WALKTHROUGH_ACTIVITIES);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String updateMasterWalkthroughActivities(masterWalkthroughActivitiesMod MWAM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_ACTIVITIES = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("UPDATE m_wtr_activities SET \n" +
                "activities_time_start = ?,\n" +
                "activities_time_end = ?,\n" +
                "edited_opt = ?\n" +
                "WHERE\n" +
                "activities_bu = ? AND\n" +
                "activities_div = ? AND\n" +
                "activities_name = ?"); 
            ps.setString(1, MWAM.getActivitiesTimeStart()+":00");
            ps.setString(2, MWAM.getActivitiesTimeEnd()+":00");
            ps.setString(3, MWAM.getEditedOpt());
            ps.setString(4, MWAM.getActivitiesBu());
            ps.setString(5, MWAM.getActivitiesDiv());
            ps.setString(6, MWAM.getActivitiesName());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success edit walkthrough activities time.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_ACTIVITIES = new JSONObject();
        
        DATA_WALKTHROUGH_ACTIVITIES.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_ACTIVITIES.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_ACTIVITIES.put(DATA_WALKTHROUGH_ACTIVITIES);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_ACTIVITIES", DATA_MASTER_WALKTHROUGH_ACTIVITIES);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String deleteMasterWalkthroughActivities(masterWalkthroughActivitiesMod MWAM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_ACTIVITIES = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("DELETE FROM m_wtr_activities WHERE activities_bu = ? AND activities_div = ? AND activities_name = ?");
            ps.setString(1, MWAM.getActivitiesBu());
            ps.setString(2, MWAM.getActivitiesDiv());
            ps.setString(3, MWAM.getActivitiesName());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success delete walkthrough activities record.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_ACTIVITIES = new JSONObject();
        
        DATA_WALKTHROUGH_ACTIVITIES.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_ACTIVITIES.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_ACTIVITIES.put(DATA_WALKTHROUGH_ACTIVITIES);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_ACTIVITIES", DATA_MASTER_WALKTHROUGH_ACTIVITIES);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
}
