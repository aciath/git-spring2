/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.masterWalkthroughAreaMod;
import dbConnection.dbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class masterWalkthroughAreaDAO {
    private Connection conn = null;
    
    public String getAllMasterWalkthroughArea(){
        String jsonResponse = "";
    
        String recid = "";
        String areaBU = "";
        String areaDiv = "";
        String wilayah = "";
        String lokasi = "";
        String isfstandOnly = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_AREA = new JSONArray();
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            String query = "SELECT recid, area_bu, area_div, wilayah, lokasi, isfstand_only FROM m_wtr_area";
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                recid = rs.getString("RECID");
                areaBU = (rs.getString("AREA_BU") != null) ? rs.getString("AREA_BU") : "";
                areaDiv = (rs.getString("AREA_DIV") != null) ? rs.getString("AREA_DIV") : "";
                wilayah = (rs.getString("WILAYAH") != null) ? rs.getString("WILAYAH") : "";
                lokasi = (rs.getString("LOKASI") != null) ? rs.getString("LOKASI") : "";
                isfstandOnly = (rs.getString("ISFSTAND_ONLY") != null) ? rs.getString("ISFSTAND_ONLY") : "";
                
                JSONObject DATA_WALKTHROUGH_AREA = new JSONObject();
                
                DATA_WALKTHROUGH_AREA.put("RECID", new String(recid));
                DATA_WALKTHROUGH_AREA.put("AREA_BU", new String(areaBU));
                DATA_WALKTHROUGH_AREA.put("AREA_DIV", new String(areaDiv));
                DATA_WALKTHROUGH_AREA.put("WILAYAH", new String(wilayah));
                DATA_WALKTHROUGH_AREA.put("LOKASI", new String(lokasi));
                DATA_WALKTHROUGH_AREA.put("ISFSTAND_ONLY", new String(isfstandOnly));
                
                DATA_MASTER_WALKTHROUGH_AREA.put(DATA_WALKTHROUGH_AREA);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_AREA", DATA_MASTER_WALKTHROUGH_AREA);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String getAllMasterWalkthroughArea(masterWalkthroughAreaMod MWAM){
        String jsonResponse = "";
        
        String recid = "";
        String areaBU = "";
        String areaDiv = "";
        String wilayah = "";
        String lokasi = "";
        String isfstandOnly = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_AREA = new JSONArray();
 
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("SELECT activities_bu, activities_div, activities_name, activities_time_start, activities_time_end, created_opt, created_dt, edited_opt, edited_bu, client_addr FROM m_wtr_activities WHERE activities_bu = ?");
            ps.setInt(1, MWAM.getRecid());
            rs = ps.executeQuery();
            
            while (rs.next()) {
                recid = rs.getString("RECID");
                areaBU = (rs.getString("AREA_BU") != null) ? rs.getString("AREA_BU") : "";
                areaDiv = (rs.getString("AREA_DIV") != null) ? rs.getString("AREA_DIV") : "";
                wilayah = (rs.getString("WILAYAH") != null) ? rs.getString("WILAYAH") : "";
                lokasi = (rs.getString("LOKASI") != null) ? rs.getString("LOKASI") : "";
                isfstandOnly = (rs.getString("ISFSTAND_ONLY") != null) ? rs.getString("ISFSTAND_ONLY") : "";
                
                JSONObject DATA_WALKTHROUGH_AREA = new JSONObject();
                
                DATA_WALKTHROUGH_AREA.put("RECID", new String(recid));
                DATA_WALKTHROUGH_AREA.put("AREA_BU", new String(areaBU));
                DATA_WALKTHROUGH_AREA.put("AREA_DIV", new String(areaDiv));
                DATA_WALKTHROUGH_AREA.put("WILAYAH", new String(wilayah));
                DATA_WALKTHROUGH_AREA.put("LOKASI", new String(lokasi));
                DATA_WALKTHROUGH_AREA.put("ISFSTAND_ONLY", new String(isfstandOnly));
                
                DATA_MASTER_WALKTHROUGH_AREA.put(DATA_WALKTHROUGH_AREA);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_AREA", DATA_MASTER_WALKTHROUGH_AREA);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String saveMasterWalkthroughArea(masterWalkthroughAreaMod MWAM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_AREA = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("INSERT INTO m_wtr_area (area_bu, area_div, wilayah, lokasi, isfstand_only) VALUES (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, MWAM.getAreaBu());
            ps.setString(2, MWAM.getAreaDiv());
            ps.setString(3, MWAM.getWilayah());
            ps.setString(4, MWAM.getLokasi());
            ps.setString(5, MWAM.getIsfstandOnly());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success add walkthrough area.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_AREA = new JSONObject();
        
        DATA_WALKTHROUGH_AREA.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_AREA.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_AREA.put(DATA_WALKTHROUGH_AREA);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_AREA", DATA_MASTER_WALKTHROUGH_AREA);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String updateMasterWalkthroughArea(masterWalkthroughAreaMod MWAM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_AREA = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("UPDATE m_wtr_area SET " +
                "area_bu = ?, " +
                "area_div = ?, " +
                "wilayah = ?, " +
                "lokasi = ?, " +
                "isfstand_only = ? " +
                "WHERE " +
                "recid = ?"); 
            ps.setString(1, MWAM.getAreaBu());
            ps.setString(2, MWAM.getAreaDiv());
            ps.setString(3, MWAM.getWilayah());
            ps.setString(4, MWAM.getLokasi());
            ps.setString(5, MWAM.getIsfstandOnly());
            ps.setString(6, String.valueOf(MWAM.getRecid()));
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success edit walkthrough area.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_AREA = new JSONObject();
        
        DATA_WALKTHROUGH_AREA.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_AREA.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_AREA.put(DATA_WALKTHROUGH_AREA);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_AREA", DATA_MASTER_WALKTHROUGH_AREA);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String deleteMasterWalkthroughArea(masterWalkthroughAreaMod MWAM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_AREA = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("DELETE FROM m_wtr_area WHERE recid = ?");
            ps.setString(1, String.valueOf(MWAM.getRecid()));
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success delete walkthrough area record.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_AREA = new JSONObject();
        
        DATA_WALKTHROUGH_AREA.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_AREA.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_AREA.put(DATA_WALKTHROUGH_AREA);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_AREA", DATA_MASTER_WALKTHROUGH_AREA);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
}
