/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.masterWalkthroughCheckMod;
import dbConnection.dbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class masterWalkthroughCheckDAO {
    private Connection conn = null;
    
    public String getAllMasterWalkthroughCheck(){
        String jsonResponse = "";
    
        int recid;
        String checkType = "";
        String userId = "";
        String cabangId = "";
        String checkDt = "";
        int checkId;
        String checkResult = "";
        int checkPoin;
        String checkNotes = "";
        int checkStat;
        int deviceId;
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_CHECK = new JSONArray();
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            String query = "SELECT recid, check_type, user_id, cabang_id, check_dt, check_id, check_result, check_poin, check_notes, check_stat, device_id, created_opt, created_dt, edited_opt, edited_dt, client_addr FROM m_wtr_check";
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                recid = rs.getInt("RECID");
                checkType = rs.getString("CHECK_TYPE");
                userId = rs.getString("USER_ID");
                cabangId = rs.getString("CABANG_ID");
                checkDt = rs.getString("CHECK_DT");
                checkId = rs.getInt("CHECK_ID");
                checkResult = rs.getString("CHECK_RESULT");
                checkPoin = rs.getInt("CHECK_POIN");
                checkNotes = rs.getString("CHECK_NOTES");
                checkStat = rs.getInt("CHECK_STAT");
                deviceId = rs.getInt("DEVICE_ID");
                createdOpt = rs.getString("CREATED_OPT");
                createdDt = rs.getString("CREATED_DT");
                editedOpt = rs.getString("EDITED_OPT");
                editedDt = rs.getString("EDITED_DT");
                clientAddr = rs.getString("CLIENT_ADDR");
                
                JSONObject DATA_WALKTHROUGH_CHECK = new JSONObject();
                
                DATA_WALKTHROUGH_CHECK.put("RECID", new Integer(recid));
                DATA_WALKTHROUGH_CHECK.put("CHECK_TYPE", new String(checkType));
                DATA_WALKTHROUGH_CHECK.put("USER_ID", new String(userId));
                DATA_WALKTHROUGH_CHECK.put("CABANG_ID", new String(cabangId));
                DATA_WALKTHROUGH_CHECK.put("CHECK_DT", new String(checkDt));
                DATA_WALKTHROUGH_CHECK.put("CHECK_ID", new Integer(checkId));
                DATA_WALKTHROUGH_CHECK.put("CHECK_RESULT", new String(checkResult));
                DATA_WALKTHROUGH_CHECK.put("CHECK_POIN", new Integer(checkPoin));
                DATA_WALKTHROUGH_CHECK.put("CHECK_NOTES", new String(checkNotes));
                DATA_WALKTHROUGH_CHECK.put("CHECK_STAT", new Integer(checkStat));
                DATA_WALKTHROUGH_CHECK.put("DEVICE_ID", new Integer(deviceId));
                DATA_WALKTHROUGH_CHECK.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_CHECK.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_CHECK.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_CHECK.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_CHECK.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_WALKTHROUGH_CHECK.put(DATA_WALKTHROUGH_CHECK);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_CHECK", DATA_MASTER_WALKTHROUGH_CHECK);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String getMasterWalkthroughCheck(masterWalkthroughCheckMod MWCM){
        String jsonResponse = "";
        
        int recid;
        String checkType = "";
        String userId = "";
        String cabangId = "";
        String checkDt = "";
        int checkId;
        String checkResult = "";
        int checkPoin;
        String checkNotes = "";
        int checkStat;
        int deviceId;
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_CHECK = new JSONArray();
 
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("SELECT recid, check_type, user_id, cabang_id, check_dt, check_id, check_result, check_poin, check_notes, check_stat, device_id, created_opt, created_dt, edited_opt, edited_dt, client_addr FROM m_wtr_check WHERE recid = ?");
            ps.setInt(1, MWCM.getRecid());
            rs = ps.executeQuery();
            
            while (rs.next()) {
                recid = rs.getInt("RECID");
                checkType = rs.getString("CHECK_TYPE");
                userId = rs.getString("USER_ID");
                cabangId = rs.getString("CABANG_ID");
                checkDt = rs.getString("CHECK_DT");
                checkId = rs.getInt("CHECK_ID");
                checkResult = rs.getString("CHECK_RESULT");
                checkPoin = rs.getInt("CHECK_POIN");
                checkNotes = rs.getString("CHECK_NOTES");
                checkStat = rs.getInt("CHECK_STAT");
                deviceId = rs.getInt("DEVICE_ID");
                createdOpt = rs.getString("CREATED_OPT");
                createdDt = rs.getString("CREATED_DT");
                editedOpt = rs.getString("EDITED_OPT");
                editedDt = rs.getString("EDITED_DT");
                clientAddr = rs.getString("CLIENT_ADDR");
                
                JSONObject DATA_WALKTHROUGH_CHECK = new JSONObject();
                
                DATA_WALKTHROUGH_CHECK.put("RECID", new Integer(recid));
                DATA_WALKTHROUGH_CHECK.put("CHECK_TYPE", new String(checkType));
                DATA_WALKTHROUGH_CHECK.put("USER_ID", new String(userId));
                DATA_WALKTHROUGH_CHECK.put("CABANG_ID", new String(cabangId));
                DATA_WALKTHROUGH_CHECK.put("CHECK_DT", new String(checkDt));
                DATA_WALKTHROUGH_CHECK.put("CHECK_ID", new Integer(checkId));
                DATA_WALKTHROUGH_CHECK.put("CHECK_RESULT", new String(checkResult));
                DATA_WALKTHROUGH_CHECK.put("CHECK_POIN", new Integer(checkPoin));
                DATA_WALKTHROUGH_CHECK.put("CHECK_NOTES", new String(checkNotes));
                DATA_WALKTHROUGH_CHECK.put("CHECK_STAT", new Integer(checkStat));
                DATA_WALKTHROUGH_CHECK.put("DEVICE_ID", new Integer(deviceId));
                DATA_WALKTHROUGH_CHECK.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_CHECK.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_CHECK.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_CHECK.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_CHECK.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_WALKTHROUGH_CHECK.put(DATA_WALKTHROUGH_CHECK);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_CHECK", DATA_MASTER_WALKTHROUGH_CHECK);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String saveMasterWalkthroughCheck(masterWalkthroughCheckMod MWCM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_CHECK = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("INSERT INTO m_wtr_check (check_type, user_id, cabang_id, check_dt, check_id, check_result, check_poin, check_notes, check_stat, device_id) VALUES (?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, MWCM.getCheckType());
            ps.setString(2, MWCM.getUserId());
            ps.setString(3, MWCM.getCabangId());
            ps.setString(4, MWCM.getCheckDt());
            ps.setInt(5, MWCM.getCheckId());
            ps.setString(6, MWCM.getCheckResult());
            ps.setInt(7, MWCM.getCheckPoin());
            ps.setString(8, MWCM.getCheckNotes());
            ps.setInt(9, MWCM.getCheckStat());
            ps.setInt(10, MWCM.getDeviceId());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success add walkthrough check data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_CHECK = new JSONObject();
        
        DATA_WALKTHROUGH_CHECK.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_CHECK.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_CHECK.put(DATA_WALKTHROUGH_CHECK);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_CHECK", DATA_MASTER_WALKTHROUGH_CHECK);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String updateMasterWalkthroughCheck(masterWalkthroughCheckMod MWCM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_CHECK = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("UPDATE m_cabang_type SET "
                    + "check_type = ?, "
                    + "user_id = ?, "
                    + "cabang_id = ?, "
                    + "check_dt = ?, "
                    + "check_id = ?, "
                    + "check_result = ?, "
                    + "check_poin = ?, "
                    + "check_notes = ?, "
                    + "check_stat = ?, "
                    + "device_id = ? "
                    + "WHERE recid = ?"); 
            ps.setString(1, MWCM.getCheckType());
            ps.setString(2, MWCM.getUserId());
            ps.setString(3, MWCM.getCabangId());
            ps.setString(4, MWCM.getCheckDt());
            ps.setInt(5, MWCM.getCheckId());
            ps.setString(6, MWCM.getCheckResult());
            ps.setInt(7, MWCM.getCheckPoin());
            ps.setString(8, MWCM.getCheckNotes());
            ps.setInt(9, MWCM.getCheckStat());
            ps.setInt(10, MWCM.getDeviceId());
            ps.setInt(11, MWCM.getRecid());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success edit walkthrough check data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_CHECK = new JSONObject();
        
        DATA_WALKTHROUGH_CHECK.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_CHECK.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_CHECK.put(DATA_WALKTHROUGH_CHECK);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_CHECK", DATA_MASTER_WALKTHROUGH_CHECK);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String deleteMasterWalkthroughCheck(masterWalkthroughCheckMod MWCM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_CHECK = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("DELETE FROM m_wtr_check WHERE recid = ?"); 
            ps.setInt(1, MWCM.getRecid());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success delete walkthrough check data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_CHECK = new JSONObject();
        
        DATA_WALKTHROUGH_CHECK.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_CHECK.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_CHECK.put(DATA_WALKTHROUGH_CHECK);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_CHECK", DATA_MASTER_WALKTHROUGH_CHECK);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
}
