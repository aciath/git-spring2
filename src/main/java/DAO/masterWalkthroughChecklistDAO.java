/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.masterWalkthroughChecklistMod;
import dbConnection.dbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class masterWalkthroughChecklistDAO {
    private Connection conn = null;
    
    public String getAllMasterWalkthroughChecklist(){
        String jsonResponse = "";
    
        int recid;
        int chkObjectId;
        int chkObjectUrut;
        String chkObjectDetail = "";
        String chkObjectDesc = "";
        String chkObjectType = "";
        int chkObjectNilai;
        String ischkObjectWt = "";
        String ischkObjectSa = "";
        String ischkObjectPict = "";
        String isActive = "";
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_CHECKLIST = new JSONArray();
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            String query = "SELECT recid, chk_object_id, chk_object_urut, chk_object_detail, chk_object_desc, chk_object_type, chk_object_nilai, ischk_object_wt, ischk_object_sa, ischk_object_pict, isactive, created_opt, created_dt, edited_opt, edited_dt, client_addr FROM m_wtr_checklist";
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                recid = rs.getInt("RECID");
                chkObjectId = rs.getInt("CHK_OBJECT_ID");
                chkObjectUrut = rs.getInt("CHK_OBJECT_URUT");
                chkObjectDetail = (rs.getString("CHK_OBJECT_DETAIL") != null) ? rs.getString("CHK_OBJECT_DETAIL") : "";
                chkObjectDesc = (rs.getString("CHK_OBJECT_DESC") != null) ? rs.getString("CHK_OBJECT_DESC") : "";
                chkObjectType = (rs.getString("CHK_OBJECT_TYPE") != null) ? rs.getString("CHK_OBJECT_TYPE") : "";
                chkObjectNilai = rs.getInt("CHK_OBJECT_NILAI");
                ischkObjectWt = (rs.getString("ISCHK_OBJECT_WT") != null) ? rs.getString("ISCHK_OBJECT_WT") : "";
                ischkObjectSa = (rs.getString("ISCHK_OBJECT_SA") != null) ? rs.getString("ISCHK_OBJECT_SA") : "";
                ischkObjectPict = (rs.getString("ISCHK_OBJECT_PICT") != null) ? rs.getString("ISCHK_OBJECT_PICT") : "";
                isActive = (rs.getString("ISACTIVE") != null) ? rs.getString("ISACTIVE") : "";
                createdOpt = (rs.getString("CREATED_OPT") != null) ? rs.getString("CREATED_OPT") : "";
                createdDt = (rs.getString("CREATED_DT") != null) ? rs.getString("CREATED_DT") : "";
                editedOpt = (rs.getString("EDITED_OPT") != null) ? rs.getString("EDITED_OPT") : "";
                editedDt = (rs.getString("EDITED_DT") != null) ? rs.getString("EDITED_DT") : "";
                clientAddr = (rs.getString("CLIENT_ADDR") != null) ? rs.getString("CLIENT_ADDR") : "";
                
                JSONObject DATA_WALKTHROUGH_CHECKLIST = new JSONObject();
                
                DATA_WALKTHROUGH_CHECKLIST.put("RECID", new Integer(recid));
                DATA_WALKTHROUGH_CHECKLIST.put("CHK_OBJECT_ID", new Integer(chkObjectId));
                DATA_WALKTHROUGH_CHECKLIST.put("CHK_OBJECT_URUT", new Integer(chkObjectUrut));
                DATA_WALKTHROUGH_CHECKLIST.put("CHK_OBJECT_DETAIL", new String(chkObjectDetail));
                DATA_WALKTHROUGH_CHECKLIST.put("CHK_OBJECT_DESC", new String(chkObjectDesc));
                DATA_WALKTHROUGH_CHECKLIST.put("CHK_OBJECT_TYPE", new String(chkObjectType));
                DATA_WALKTHROUGH_CHECKLIST.put("CHK_OBJECT_NILAI", new Integer(chkObjectNilai));
                DATA_WALKTHROUGH_CHECKLIST.put("ISCHK_OBJECT_WT", new String(ischkObjectWt));
                DATA_WALKTHROUGH_CHECKLIST.put("ISCHK_OBJECT_SA", new String(ischkObjectSa));
                DATA_WALKTHROUGH_CHECKLIST.put("ISCHK_OBJECT_PICT", new String(ischkObjectPict));
                DATA_WALKTHROUGH_CHECKLIST.put("ISACTIVE", new String(isActive));
                DATA_WALKTHROUGH_CHECKLIST.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_CHECKLIST.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_CHECKLIST.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_CHECKLIST.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_CHECKLIST.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_WALKTHROUGH_CHECKLIST.put(DATA_WALKTHROUGH_CHECKLIST);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_CHECKLIST", DATA_MASTER_WALKTHROUGH_CHECKLIST);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String getMasterWalkthroughChecklist(masterWalkthroughChecklistMod MWCM){
        String jsonResponse = "";
        
        int recid;
        int chkObjectId;
        int chkObjectUrut;
        String chkObjectDetail = "";
        String chkObjectDesc = "";
        String chkObjectType = "";
        int chkObjectNilai;
        String ischkObjectWt = "";
        String ischkObjectSa = "";
        String ischkObjectPict = "";
        String isActive = "";
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_CHECKLIST = new JSONArray();
 
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("SELECT recid, chk_object_id, chk_object_urut, chk_object_detail, chk_object_desc, chk_object_type, chk_object_nilai, ischk_object_wt, ischk_object_sa, ischk_object_pict, isactive, created_opt, created_dt, edited_opt, edited_dt, client_addr WHERE recid = ?");
            ps.setString(1, String.valueOf(MWCM.getRecid()));
            rs = ps.executeQuery();
            
            while (rs.next()) {
                recid = rs.getInt("RECID");
                chkObjectId = rs.getInt("CHK_OBJECT_ID");
                chkObjectUrut = rs.getInt("CHK_OBJECT_URUT");
                chkObjectDetail = (rs.getString("CHK_OBJECT_DETAIL") != null) ? rs.getString("CHK_OBJECT_DETAIL") : "";
                chkObjectDesc = (rs.getString("CHK_OBJECT_DESC") != null) ? rs.getString("CHK_OBJECT_DESC") : "";
                chkObjectType = (rs.getString("CHK_OBJECT_TYPE") != null) ? rs.getString("CHK_OBJECT_TYPE") : "";
                chkObjectNilai = rs.getInt("CHK_OBJECT_NILAI");
                ischkObjectWt = (rs.getString("ISCHK_OBJECT_WT") != null) ? rs.getString("ISCHK_OBJECT_WT") : "";
                ischkObjectSa = (rs.getString("ISCHK_OBJECT_SA") != null) ? rs.getString("ISCHK_OBJECT_SA") : "";
                ischkObjectPict = (rs.getString("ISCHK_OBJECT_PICT") != null) ? rs.getString("ISCHK_OBJECT_PICT") : "";
                isActive = (rs.getString("ISACTIVE") != null) ? rs.getString("ISACTIVE") : "";
                createdOpt = (rs.getString("CREATED_OPT") != null) ? rs.getString("CREATED_OPT") : "";
                createdDt = (rs.getString("CREATED_DT") != null) ? rs.getString("CREATED_DT") : "";
                editedOpt = (rs.getString("EDITED_OPT") != null) ? rs.getString("EDITED_OPT") : "";
                editedDt = (rs.getString("EDITED_DT") != null) ? rs.getString("EDITED_DT") : "";
                clientAddr = (rs.getString("CLIENT_ADDR") != null) ? rs.getString("CLIENT_ADDR") : "";
                
                JSONObject DATA_WALKTHROUGH_CHECKLIST = new JSONObject();
                
                DATA_WALKTHROUGH_CHECKLIST.put("RECID", new Integer(recid));
                DATA_WALKTHROUGH_CHECKLIST.put("CHK_OBJECT_ID", new Integer(chkObjectId));
                DATA_WALKTHROUGH_CHECKLIST.put("CHK_OBJECT_URUT", new Integer(chkObjectUrut));
                DATA_WALKTHROUGH_CHECKLIST.put("CHK_OBJECT_DETAIL", new String(chkObjectDetail));
                DATA_WALKTHROUGH_CHECKLIST.put("CHK_OBJECT_DESC", new String(chkObjectDesc));
                DATA_WALKTHROUGH_CHECKLIST.put("CHK_OBJECT_TYPE", new String(chkObjectType));
                DATA_WALKTHROUGH_CHECKLIST.put("CHK_OBJECT_NILAI", new Integer(chkObjectNilai));
                DATA_WALKTHROUGH_CHECKLIST.put("ISCHK_OBJECT_WT", new String(ischkObjectWt));
                DATA_WALKTHROUGH_CHECKLIST.put("ISCHK_OBJECT_SA", new String(ischkObjectSa));
                DATA_WALKTHROUGH_CHECKLIST.put("ISCHK_OBJECT_PICT", new String(ischkObjectPict));
                DATA_WALKTHROUGH_CHECKLIST.put("ISACTIVE", new String(isActive));
                DATA_WALKTHROUGH_CHECKLIST.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_CHECKLIST.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_CHECKLIST.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_CHECKLIST.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_CHECKLIST.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_WALKTHROUGH_CHECKLIST.put(DATA_WALKTHROUGH_CHECKLIST);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_CHECKLIST", DATA_MASTER_WALKTHROUGH_CHECKLIST);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String saveMasterWalkthroughChecklist(masterWalkthroughChecklistMod MWCM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_CHECKLIST = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("INSERT INTO m_wtr_checklist (chk_object_id, chk_object_urut, chk_object_detail, chk_object_desc, chk_object_type, chk_object_nilai, ischk_object_wt, ischk_object_sa, ischk_object_pict, isactive) VALUES (?,?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, MWCM.getChkObjectId());
            ps.setInt(2, MWCM.getChkObjectUrut());
            ps.setString(3, MWCM.getChkObjectDetail());
            ps.setString(4, MWCM.getChkObjectDesc());
            ps.setString(5, MWCM.getChkObjectType());
            ps.setInt(6, MWCM.getChkObjectNilai());
            ps.setString(7, MWCM.getIschkObjectWt());
            ps.setString(8, MWCM.getIschkObjectSa());
            ps.setString(9, MWCM.getIschkObjectPict());
            ps.setString(10, MWCM.getIsactive());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success add walkthrough checklist data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_CHECKLIST = new JSONObject();
        
        DATA_WALKTHROUGH_CHECKLIST.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_CHECKLIST.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_CHECKLIST.put(DATA_WALKTHROUGH_CHECKLIST);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_CHECKLIST", DATA_MASTER_WALKTHROUGH_CHECKLIST);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String updateMasterWalkthroughChecklist(masterWalkthroughChecklistMod MWCM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_CHECKLIST = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("UPDATE m_wtr_checklist SET \n" +
                "chk_object_id = ?,\n" +
                "chk_object_urut = ?,\n" +
                "chk_object_detail = ?,\n" +
                "chk_object_desc = ?,\n" +
                "chk_object_type = ?,\n" +
                "chk_object_nilai = ?,\n" +
                "ischk_object_wt = ?,\n" +
                "ischk_object_sa = ?,\n" +
                "ischk_object_pict = ?,\n" +
                "isactive = ?\n" +
                "WHERE recid = ?"); 
            ps.setInt(1, MWCM.getChkObjectId());
            ps.setInt(2, MWCM.getChkObjectUrut());
            ps.setString(3, MWCM.getChkObjectDetail());
            ps.setString(4, MWCM.getChkObjectDesc());
            ps.setString(5, MWCM.getChkObjectType());
            ps.setInt(6, MWCM.getChkObjectNilai());
            ps.setString(7, MWCM.getIschkObjectWt());
            ps.setString(8, MWCM.getIschkObjectSa());
            ps.setString(9, MWCM.getIschkObjectPict());
            ps.setString(10, MWCM.getIsactive());
            ps.setInt(11, MWCM.getRecid());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success edit walkthrough checklist data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_CHECKLIST = new JSONObject();
        
        DATA_WALKTHROUGH_CHECKLIST.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_CHECKLIST.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_CHECKLIST.put(DATA_WALKTHROUGH_CHECKLIST);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_CHECKLIST", DATA_MASTER_WALKTHROUGH_CHECKLIST);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String deleteMasterWalkthroughChecklist(masterWalkthroughChecklistMod MWCM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_CHECKLIST = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("DELETE FROM m_wtr_checklist WHERE recid = ?"); 
            ps.setInt(1, MWCM.getRecid());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success delete walkthrough checklist.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_CHECKLIST = new JSONObject();
        
        DATA_WALKTHROUGH_CHECKLIST.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_CHECKLIST.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_CHECKLIST.put(DATA_WALKTHROUGH_CHECKLIST);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_CHECKLIST", DATA_MASTER_WALKTHROUGH_CHECKLIST);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
}
