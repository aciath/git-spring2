/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.masterWalkthroughDivMod;
import dbConnection.dbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class masterWalkthroughDivDAO {
    private Connection conn = null;
    
    public String getAllMasterWalkthroughDiv(){
        String jsonResponse = "";
    
        String divId = "";
        String divDescription = "";
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_DIV = new JSONArray();
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            String query = "SELECT div_id, div_description, created_opt, created_dt, edited_opt, edited_dt, client_addr FROM m_wtr_div";
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                divId = (rs.getString("DIV_ID") != null) ? rs.getString("DIV_ID") : "";
                divDescription = (rs.getString("DIV_DESCRIPTION") != null) ? rs.getString("DIV_DESCRIPTION") : "";
                createdOpt = (rs.getString("CREATED_OPT") != null) ? rs.getString("CREATED_OPT") : "";
                createdDt = (rs.getString("CREATED_DT") != null) ? rs.getString("CREATED_DT") : "";
                editedOpt = (rs.getString("EDITED_OPT") != null) ? rs.getString("EDITED_OPT") : "";
                editedDt = (rs.getString("EDITED_DT") != null) ? rs.getString("EDITED_DT") : "";
                clientAddr = (rs.getString("CLIENT_ADDR") != null) ? rs.getString("CLIENT_ADDR") : "";
                
                JSONObject DATA_WALKTHROUGH_DIV = new JSONObject();
                
                DATA_WALKTHROUGH_DIV.put("DIV_ID", new String(divId));
                DATA_WALKTHROUGH_DIV.put("DIV_DESCRIPTION", new String(divDescription));
                DATA_WALKTHROUGH_DIV.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_DIV.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_DIV.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_DIV.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_DIV.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_WALKTHROUGH_DIV.put(DATA_WALKTHROUGH_DIV);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_DIV", DATA_MASTER_WALKTHROUGH_DIV);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String getMasterWalkthroughDiv(masterWalkthroughDivMod MWDM){
        String jsonResponse = "";
        
        String divId = "";
        String divDescription = "";
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_DIV = new JSONArray();
 
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("SELECT div_id, div_description, created_opt, created_dt, edited_opt, edited_dt, client_addr FROM m_wtr_div WHERE div_id = ?");
            ps.setString(1, MWDM.getDivId());
            rs = ps.executeQuery();
            
            while (rs.next()) {
                divId = rs.getString("DIV_ID");
                divDescription = rs.getString("DIV_DESCRIPTION");
                createdOpt = rs.getString("CREATED_OPT");
                createdDt = rs.getString("CREATED_DT");
                editedOpt = rs.getString("EDITED_OPT");
                editedDt = rs.getString("EDITED_DT");
                clientAddr = rs.getString("CLIENT_ADDR");
                
                JSONObject DATA_WALKTHROUGH_DIV = new JSONObject();
                
                DATA_WALKTHROUGH_DIV.put("DIV_ID", new String(divId));
                DATA_WALKTHROUGH_DIV.put("DIV_DESCRIPTION", new String(divDescription));
                DATA_WALKTHROUGH_DIV.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_DIV.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_DIV.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_DIV.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_DIV.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_WALKTHROUGH_DIV.put(DATA_WALKTHROUGH_DIV);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_DIV", DATA_MASTER_WALKTHROUGH_DIV);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String saveMasterWalkthroughDiv(masterWalkthroughDivMod MWDM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_DIV = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("INSERT INTO m_wtr_div (div_id, div_description, created_opt) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, MWDM.getDivId());
            ps.setString(2, MWDM.getDivDescription());
            ps.setString(3, MWDM.getCreatedOpt());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success add walkthrough div.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_DIV = new JSONObject();
        
        DATA_WALKTHROUGH_DIV.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_DIV.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_DIV.put(DATA_WALKTHROUGH_DIV);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_DIV", DATA_MASTER_WALKTHROUGH_DIV);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String updateMasterWalkthroughDiv(masterWalkthroughDivMod MWDM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_DIV = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("UPDATE m_wtr_div SET div_description = ? WHERE div_id = ?"); 
            ps.setString(1, MWDM.getDivDescription());
            ps.setString(2, MWDM.getDivId());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success edit walkthrough div data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_DIV = new JSONObject();
        
        DATA_WALKTHROUGH_DIV.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_DIV.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_DIV.put(DATA_WALKTHROUGH_DIV);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_DIV", DATA_MASTER_WALKTHROUGH_DIV);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String deleteMasterCabangType(masterWalkthroughDivMod MWDM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_DIV = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("DELETE FROM m_wtr_div WHERE div_id = ?"); 
            ps.setString(1, MWDM.getDivId());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success delete walkthrough div data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_DIV = new JSONObject();
        
        DATA_WALKTHROUGH_DIV.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_DIV.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_DIV.put(DATA_WALKTHROUGH_DIV);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_DIV", DATA_MASTER_WALKTHROUGH_DIV);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
}
