/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.masterWalkthroughLoginMod;
import dbConnection.dbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class masterWalkthroughLoginDAO {
    private Connection conn = null;
    
    public String getAllMasterWalkthroughLogin(){
        String jsonResponse = "";
    
        int recid;
        String loginName = "";
        int deviceId;
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        String isActive = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_LOGIN = new JSONArray();
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            String query = "SELECT recid, login_name, device_id, created_opt, created_dt, edited_opt, edited_dt, client_addr, isactive FROM m_wtr_login";
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                recid = rs.getInt("RECID");
                loginName = (rs.getString("LOGIN_NAME") != null) ? rs.getString("LOGIN_NAME") : "";
                deviceId = rs.getInt("DEVICE_ID");
                createdOpt = (rs.getString("CREATED_OPT") != null) ? rs.getString("CREATED_OPT") : "";
                createdDt = (rs.getString("CREATED_DT") != null) ? rs.getString("CREATED_DT") : "";
                editedOpt = (rs.getString("EDITED_OPT") != null) ? rs.getString("EDITED_OPT") : "";
                editedDt = (rs.getString("EDITED_DT") != null) ? rs.getString("EDITED_DT") : "";
                clientAddr = (rs.getString("CLIENT_ADDR") != null) ? rs.getString("CLIENT_ADDR") : "";
                isActive = (rs.getString("ISACTIVE") != null) ? rs.getString("ISACTIVE") : "";
                
                JSONObject DATA_WALKTHROUGH_LOGIN = new JSONObject();
                
                DATA_WALKTHROUGH_LOGIN.put("RECID", new Integer(recid));
                DATA_WALKTHROUGH_LOGIN.put("LOGIN_NAME", new String(loginName));
                DATA_WALKTHROUGH_LOGIN.put("DEVICE_ID", new Integer(deviceId));
                DATA_WALKTHROUGH_LOGIN.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_LOGIN.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_LOGIN.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_LOGIN.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_LOGIN.put("CLIENT_ADDR", new String(clientAddr));
                DATA_WALKTHROUGH_LOGIN.put("ISACTIVE", new String(isActive));
                
                DATA_MASTER_WALKTHROUGH_LOGIN.put(DATA_WALKTHROUGH_LOGIN);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_LOGIN", DATA_MASTER_WALKTHROUGH_LOGIN);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String getMasterWalkthroughLogin(masterWalkthroughLoginMod MWLM){
        String jsonResponse = "";
        
        int recid;
        String loginName = "";
        int deviceId;
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        String isActive = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_LOGIN = new JSONArray();
 
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("SELECT recid, login_name, device_id, created_opt, created_dt, edited_opt, edited_dt, client_addr, isactive FROM m_wtr_login WHERE recid = ?");
            ps.setString(1, String.valueOf(MWLM.getRecid()));
            rs = ps.executeQuery();
            
            while (rs.next()) {
                recid = rs.getInt("RECID");
                loginName = (rs.getString("LOGIN_NAME") != null) ? rs.getString("LOGIN_NAME") : "";
                deviceId = rs.getInt("DEVICE_ID");
                createdOpt = (rs.getString("CREATED_OPT") != null) ? rs.getString("CREATED_OPT") : "";
                createdDt = (rs.getString("CREATED_DT") != null) ? rs.getString("CREATED_DT") : "";
                editedOpt = (rs.getString("EDITED_OPT") != null) ? rs.getString("EDITED_OPT") : "";
                editedDt = (rs.getString("EDITED_DT") != null) ? rs.getString("EDITED_DT") : "";
                clientAddr = (rs.getString("CLIENT_ADDR") != null) ? rs.getString("CLIENT_ADDR") : "";
                isActive = (rs.getString("ISACTIVE") != null) ? rs.getString("ISACTIVE") : "";
                
                JSONObject DATA_WALKTHROUGH_LOGIN = new JSONObject();
                
                DATA_WALKTHROUGH_LOGIN.put("RECID", new Integer(recid));
                DATA_WALKTHROUGH_LOGIN.put("LOGIN_NAME", new String(loginName));
                DATA_WALKTHROUGH_LOGIN.put("DEVICE_ID", new Integer(deviceId));
                DATA_WALKTHROUGH_LOGIN.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_LOGIN.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_LOGIN.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_LOGIN.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_LOGIN.put("CLIENT_ADDR", new String(clientAddr));
                DATA_WALKTHROUGH_LOGIN.put("ISACTIVE", new String(isActive));
                
                DATA_MASTER_WALKTHROUGH_LOGIN.put(DATA_WALKTHROUGH_LOGIN);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_LOGIN", DATA_MASTER_WALKTHROUGH_LOGIN);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String saveMasterWalkthroughLogin(masterWalkthroughLoginMod MWLM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_LOGIN = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("INSERT INTO m_wtr_login (login_name, device_id, created_opt) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, MWLM.getLoginName());
            ps.setInt(2, MWLM.getDeviceId());
            ps.setString(3, MWLM.getCreatedOpt());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success add walkthrough login data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_LOGIN = new JSONObject();
        
        DATA_WALKTHROUGH_LOGIN.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_LOGIN.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_LOGIN.put(DATA_WALKTHROUGH_LOGIN);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_LOGIN", DATA_MASTER_WALKTHROUGH_LOGIN);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String updateMasterWalkthroughLogin(masterWalkthroughLoginMod MWLM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_LOGIN = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("UPDATE m_wtr_login SET login_name = ?, device_id = ?, edited_opt = ? WHERE recid = ?"); 
            ps.setString(1, MWLM.getLoginName());
            ps.setInt(2, MWLM.getDeviceId());
            ps.setString(3, MWLM.getEditedOpt());
            ps.setInt(4, MWLM.getRecid());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success edit walkthrough login data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_LOGIN = new JSONObject();
        
        DATA_WALKTHROUGH_LOGIN.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_LOGIN.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_LOGIN.put(DATA_WALKTHROUGH_LOGIN);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_LOGIN", DATA_MASTER_WALKTHROUGH_LOGIN);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String deleteMasterWalkthroughLogin(masterWalkthroughLoginMod MWLM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_LOGIN = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("DELETE FROM m_wtr_login WHERE recid = ?"); 
            ps.setInt(1, MWLM.getRecid());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success delete walkthrough login data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_LOGIN = new JSONObject();
        
        DATA_WALKTHROUGH_LOGIN.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_LOGIN.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_LOGIN.put(DATA_WALKTHROUGH_LOGIN);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_LOGIN", DATA_MASTER_WALKTHROUGH_LOGIN);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
}
