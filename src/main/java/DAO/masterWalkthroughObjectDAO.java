/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.masterDeviceValidationMod;
import Model.masterWalkthroughObjectMod;
import dbConnection.dbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class masterWalkthroughObjectDAO {
    private Connection conn = null;
    
    public String getAllMasterWalkthroughObject(){
        String jsonResponse = "";
    
        int recid;
        String objBu = "";
        String objDiv = "";
        String objActivities = "";
        String objWilayah = "";
        String objLokasi = "";
        String objName = "";
        int objUrut;
        String objPic = "";
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_OBJECT = new JSONArray();
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            String query = "SELECT recid, obj_bu, obj_div, obj_activities, obj_wilayah, obj_lokasi, obj_name, obj_urut, obj_pic, created_opt, created_dt, edited_opt, edited_dt, client_addr FROM m_wtr_object";
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                recid = rs.getInt("RECID");
                objBu = (rs.getString("OBJ_BU") != null) ? rs.getString("OBJ_BU") : "";
                objDiv = (rs.getString("OBJ_DIV") != null) ? rs.getString("OBJ_DIV") : "";
                objActivities = (rs.getString("OBJ_ACTIVITIES") != null) ? rs.getString("OBJ_ACTIVITIES") : "";
                objWilayah = (rs.getString("OBJ_WILAYAH") != null) ? rs.getString("OBJ_WILAYAH") : "";
                objLokasi = (rs.getString("OBJ_LOKASI") != null) ? rs.getString("OBJ_LOKASI") : "";
                objName = (rs.getString("OBJ_NAME") != null) ? rs.getString("OBJ_NAME") : "";
                objUrut = rs.getInt("OBJ_URUT");
                objPic = (rs.getString("OBJ_PIC") != null) ? rs.getString("OBJ_PIC") : "";
                createdOpt = (rs.getString("CREATED_OPT") != null) ? rs.getString("CREATED_OPT") : "";
                createdDt = (rs.getString("CREATED_DT") != null) ? rs.getString("CREATED_DT") : "";
                editedOpt = (rs.getString("EDITED_OPT") != null) ? rs.getString("EDITED_OPT") : "";
                editedDt = (rs.getString("EDITED_DT") != null) ? rs.getString("EDITED_DT") : "";
                clientAddr = (rs.getString("CLIENT_ADDR") != null) ? rs.getString("CLIENT_ADDR") : "";
                
                JSONObject DATA_WALKTHROUGH_OBJECT = new JSONObject();
                
                DATA_WALKTHROUGH_OBJECT.put("RECID", new Integer(recid));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_BU", new String(objBu));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_DIV", new String(objDiv));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_ACTIVITIES", new String(objActivities));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_WILAYAH", new String(objWilayah));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_LOKASI", new String(objLokasi));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_NAME", new String(objName));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_URUT", new Integer(objUrut));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_PIC", new String(objPic));
                DATA_WALKTHROUGH_OBJECT.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_OBJECT.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_OBJECT.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_OBJECT.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_OBJECT.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_WALKTHROUGH_OBJECT.put(DATA_WALKTHROUGH_OBJECT);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_OBJECT", DATA_MASTER_WALKTHROUGH_OBJECT);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String getMasterWalkthroughObject(masterWalkthroughObjectMod MWOM){
        String jsonResponse = "";
    
        int recid;
        String objBu = "";
        String objDiv = "";
        String objActivities = "";
        String objWilayah = "";
        String objLokasi = "";
        String objName = "";
        int objUrut;
        String objPic = "";
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_OBJECT = new JSONArray();
 
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("SELECT recid, obj_bu, obj_div, obj_activities, obj_wilayah, obj_lokasi, obj_name, obj_urut, obj_pic, created_opt, created_dt, edited_opt, edited_dt, client_addr FROM m_wtr_object WHERE recid = ?");
            ps.setString(1, String.valueOf(MWOM.getRecid()));
            rs = ps.executeQuery();
            
            while (rs.next()) {
                recid = rs.getInt("RECID");
                objBu = (rs.getString("OBJ_BU") != null) ? rs.getString("OBJ_BU") : "";
                objDiv = (rs.getString("OBJ_DIV") != null) ? rs.getString("OBJ_DIV") : "";
                objActivities = (rs.getString("OBJ_ACTIVITIES") != null) ? rs.getString("OBJ_ACTIVITIES") : "";
                objWilayah = (rs.getString("OBJ_WILAYAH") != null) ? rs.getString("OBJ_WILAYAH") : "";
                objLokasi = (rs.getString("OBJ_LOKASI") != null) ? rs.getString("OBJ_LOKASI") : "";
                objName = (rs.getString("OBJ_NAME") != null) ? rs.getString("OBJ_NAME") : "";
                objUrut = rs.getInt("OBJ_URUT");
                objPic = (rs.getString("OBJ_PIC") != null) ? rs.getString("OBJ_PIC") : "";
                createdOpt = (rs.getString("CREATED_OPT") != null) ? rs.getString("CREATED_OPT") : "";
                createdDt = (rs.getString("CREATED_DT") != null) ? rs.getString("CREATED_DT") : "";
                editedOpt = (rs.getString("EDITED_OPT") != null) ? rs.getString("EDITED_OPT") : "";
                editedDt = (rs.getString("EDITED_DT") != null) ? rs.getString("EDITED_DT") : "";
                clientAddr = (rs.getString("CLIENT_ADDR") != null) ? rs.getString("CLIENT_ADDR") : "";
                
                JSONObject DATA_WALKTHROUGH_OBJECT = new JSONObject();
                
                DATA_WALKTHROUGH_OBJECT.put("RECID", new Integer(recid));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_BU", new String(objBu));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_DIV", new String(objDiv));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_ACTIVITIES", new String(objActivities));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_WILAYAH", new String(objWilayah));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_LOKASI", new String(objLokasi));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_NAME", new String(objName));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_URUT", new Integer(objUrut));
                DATA_WALKTHROUGH_OBJECT.put("OBJ_PIC", new String(objPic));
                DATA_WALKTHROUGH_OBJECT.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_OBJECT.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_OBJECT.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_OBJECT.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_OBJECT.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_WALKTHROUGH_OBJECT.put(DATA_WALKTHROUGH_OBJECT);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_OBJECT", DATA_MASTER_WALKTHROUGH_OBJECT);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String saveMasterWalkthroughObject(masterWalkthroughObjectMod MWOM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_OBJECT = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("INSERT INTO m_wtr_object (obj_bu, obj_div, obj_activities, obj_wilayah, obj_lokasi, obj_name, obj_urut, obj_pic, created_opt) VALUES (?,?,?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, MWOM.getObjBu());
            ps.setString(2, MWOM.getObjDiv());
            ps.setString(3, MWOM.getObjActivities());
            ps.setString(4, MWOM.getObjWilayah());
            ps.setString(5, MWOM.getObjLokasi());
            ps.setString(6, MWOM.getObjName());
            ps.setInt(7, MWOM.getObjUrut());
            ps.setString(8, MWOM.getObjPic());
            ps.setString(9, MWOM.getCreatedOpt());
            
            // date now
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
            String stringDate = ft.format(dNow);
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success add new walkthrough object.";
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_OBJECT = new JSONObject();
        
        DATA_WALKTHROUGH_OBJECT.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_OBJECT.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_OBJECT.put(DATA_WALKTHROUGH_OBJECT);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_OBJECT", DATA_MASTER_WALKTHROUGH_OBJECT);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String updateMasterWalkthroughObject(masterWalkthroughObjectMod MWOM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_OBJECT = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
        String stringDate = ft.format(dNow);
            
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
            
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("UPDATE m_wtr_object SET "
                    + "obj_bu = ?, "
                    + "obj_div = ?, "
                    + "obj_activities = ?, "
                    + "obj_wilayah = ?, "
                    + "obj_lokasi = ?, "
                    + "obj_name = ?, "
                    + "obj_urut = ?, "
                    + "obj_pic = ? "
                    + "WHERE recid = ?"); 
            ps.setString(1, MWOM.getObjBu());
            ps.setString(2, MWOM.getObjDiv());
            ps.setString(3, MWOM.getObjActivities());
            ps.setString(4, MWOM.getObjWilayah());
            ps.setString(5, MWOM.getObjLokasi());
            ps.setString(6, MWOM.getObjName());
            ps.setInt(7, MWOM.getObjUrut());
            ps.setString(8, MWOM.getObjPic());
            ps.setInt(9, MWOM.getRecid());

            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Sukses mengubah data master walkthrough object.";
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
            messageResult = stringDate+" - "+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_OBJECT = new JSONObject();
        
        DATA_WALKTHROUGH_OBJECT.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_OBJECT.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_OBJECT.put(DATA_WALKTHROUGH_OBJECT);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_OBJECT", DATA_MASTER_WALKTHROUGH_OBJECT);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String deleteMasterWalkthroughObject(masterWalkthroughObjectMod MWOM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_OBJECT = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("DELETE FROM m_wtr_object WHERE recid = ?"); 
            ps.setInt(1, MWOM.getRecid());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success delete master walkthrough object data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_OBJECT = new JSONObject();
        
        DATA_WALKTHROUGH_OBJECT.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_OBJECT.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_OBJECT.put(DATA_WALKTHROUGH_OBJECT);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_OBJECT", DATA_MASTER_WALKTHROUGH_OBJECT);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
}
