/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.masterWalkthroughUserMod;
import dbConnection.dbConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class masterWalkthroughUserDAO {
    private Connection conn = null;
    
    public String getAllMasterWalkthroughUser(){
        String jsonResponse = "";
    
        int recid;
        String userId = "";
        String userDiv = "";
        String userArea = "";
        String isallowWt = "";
        String isallowSa = "";
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_USER = new JSONArray();
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            String query = "SELECT recid, user_id, user_div, user_area, isallow_wt, isallow_sa, created_opt, created_dt, edited_opt, edited_dt, client_addr FROM m_wtr_user";
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                recid = rs.getInt("RECID");
                userId = (rs.getString("USER_ID") != null) ? rs.getString("USER_ID") : "";
                userDiv = (rs.getString("USER_DIV") != null) ? rs.getString("USER_DIV") : "";
                userArea = (rs.getString("USER_AREA") != null) ? rs.getString("USER_AREA") : "";
                isallowWt = (rs.getString("ISALLOW_WT") != null) ? rs.getString("ISALLOW_WT") : "";
                isallowSa = (rs.getString("ISALLOW_SA") != null) ? rs.getString("ISALLOW_SA") : "";
                createdOpt = (rs.getString("CREATED_OPT") != null) ? rs.getString("CREATED_OPT") : "";
                createdDt = (rs.getString("CREATED_DT") != null) ? rs.getString("CREATED_DT") : "";
                editedOpt = (rs.getString("EDITED_OPT") != null) ? rs.getString("EDITED_OPT") : "";
                editedDt = (rs.getString("EDITED_DT") != null) ? rs.getString("EDITED_DT") : "";
                clientAddr = (rs.getString("CLIENT_ADDR") != null) ? rs.getString("CLIENT_ADDR") : "";
                
                JSONObject DATA_WALKTHROUGH_USER = new JSONObject();
                
                DATA_WALKTHROUGH_USER.put("RECID", new Integer(recid));
                DATA_WALKTHROUGH_USER.put("USER_ID", new String(userId));
                DATA_WALKTHROUGH_USER.put("USER_DIV", new String(userDiv));
                DATA_WALKTHROUGH_USER.put("USER_AREA", new String(userArea));
                DATA_WALKTHROUGH_USER.put("ISALLOW_WT", new String(isallowWt));
                DATA_WALKTHROUGH_USER.put("ISALLOW_SA", new String(isallowSa));
                DATA_WALKTHROUGH_USER.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_USER.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_USER.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_USER.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_USER.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_WALKTHROUGH_USER.put(DATA_WALKTHROUGH_USER);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_USER", DATA_MASTER_WALKTHROUGH_USER);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String getMasterCabangType(masterWalkthroughUserMod MWUM){
        String jsonResponse = "";
        
        int recid;
        String userId = "";
        String userDiv = "";
        String userArea = "";
        String isallowWt = "";
        String isallowSa = "";
        String createdOpt = "";
        String createdDt = "";
        String editedOpt = "";
        String editedDt = "";
        String clientAddr = "";
        
        Statement stmt = null;
        ResultSet rs;
        
        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_USER = new JSONArray();
 
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("SELECT recid, user_id, user_div, user_area, isallow_wt, isallow_sa, created_opt, created_dt, edited_opt, edited_dt, client_addr FROM m_wtr_user WHERE recid = ?");
            ps.setInt(1, MWUM.getRecid());
            rs = ps.executeQuery();
            
            while (rs.next()) {
                recid = rs.getInt("RECID");
                userId = rs.getString("USER_ID");
                userDiv = rs.getString("USER_DIV");
                userArea = rs.getString("USER_AREA");
                isallowWt = rs.getString("ISALLOW_WT");
                isallowSa = rs.getString("ISALLOW_SA");
                createdOpt = rs.getString("CREATED_OPT");
                createdDt = rs.getString("CREATED_DT");
                editedOpt = rs.getString("EDITED_OPT");
                editedDt = rs.getString("EDITED_DT");
                clientAddr = rs.getString("CLIENT_ADDR");
                
                JSONObject DATA_WALKTHROUGH_USER = new JSONObject();
                
                DATA_WALKTHROUGH_USER.put("RECID", new Integer(recid));
                DATA_WALKTHROUGH_USER.put("USER_ID", new String(userId));
                DATA_WALKTHROUGH_USER.put("USER_DIV", new String(userDiv));
                DATA_WALKTHROUGH_USER.put("USER_AREA", new String(userArea));
                DATA_WALKTHROUGH_USER.put("ISALLOW_WT", new String(isallowWt));
                DATA_WALKTHROUGH_USER.put("ISALLOW_SA", new String(isallowSa));
                DATA_WALKTHROUGH_USER.put("CREATED_OPT", new String(createdOpt));
                DATA_WALKTHROUGH_USER.put("CREATED_DT", new String(createdDt));
                DATA_WALKTHROUGH_USER.put("EDITED_OPT", new String(editedOpt));
                DATA_WALKTHROUGH_USER.put("EDITED_DT", new String(editedDt));
                DATA_WALKTHROUGH_USER.put("CLIENT_ADDR", new String(clientAddr));
                
                DATA_MASTER_WALKTHROUGH_USER.put(DATA_WALKTHROUGH_USER);
            }
            
            JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_USER", DATA_MASTER_WALKTHROUGH_USER);
            jsonResponse += JSONObjectRoot.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        return jsonResponse;
    }
    
    public String saveMasterWalkthroughUser(masterWalkthroughUserMod MWUM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_USER = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
        String stringDate = ft.format(dNow);
            
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("INSERT INTO m_wtr_user (user_id, user_div, user_area, isallow_wt, isallow_sa, created_opt, created_dt) VALUES (?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, MWUM.getUserId());
            ps.setString(2, MWUM.getUserDiv());
            ps.setString(3, MWUM.getUserArea());
            ps.setString(4, MWUM.getIsallowWt());
            ps.setString(5, MWUM.getIsallowSa());
            ps.setString(6, MWUM.getCreatedOpt());
            ps.setString(7, stringDate);
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success add user data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_USER = new JSONObject();
        
        DATA_WALKTHROUGH_USER.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_USER.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_USER.put(DATA_WALKTHROUGH_USER);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_USER", DATA_MASTER_WALKTHROUGH_USER);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String updateMasterWalkthroughUser(masterWalkthroughUserMod MWUM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_USER = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
        String stringDate = ft.format(dNow);
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("UPDATE m_wtr_user SET "
                    + "user_id = ?, "
                    + "user_div = ?, "
                    + "user_area = ?, "
                    + "isallow_wt = ?,"
                    + "isallow_sa = ?,"
                    + "edited_opt = ?,"
                    + "edited_dt = ? "
                    + " WHERE recid = ?"); 
            ps.setString(1, MWUM.getUserId());
            ps.setString(2, MWUM.getUserDiv());
            ps.setString(3, MWUM.getUserArea());
            ps.setString(4, MWUM.getIsallowWt());
            ps.setString(5, MWUM.getIsallowSa());
            ps.setString(6, MWUM.getEditedOpt());
            ps.setString(7, stringDate);
            ps.setInt(8, MWUM.getRecid());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success edit walkthrough user data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_USER = new JSONObject();
        
        DATA_WALKTHROUGH_USER.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_USER.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_USER.put(DATA_WALKTHROUGH_USER);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_USER", DATA_MASTER_WALKTHROUGH_USER);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
    
    public String deleteMasterWalkthroughUser(masterWalkthroughUserMod MWUM) throws JSONException{
        String jsonResponse = "";
        
        Statement stmt = null;

        JSONObject JSONObjectRoot = new JSONObject();
        JSONArray DATA_MASTER_WALKTHROUGH_USER = new JSONArray();
        
        boolean result = false;
        String messageResult = "";
        
        try {
            dbConnection DC = new dbConnection();
            conn = DC.getConnection();
             
            stmt = conn.createStatement();
            PreparedStatement ps = this.conn.prepareStatement("DELETE FROM m_wtr_user WHERE recid = ?"); 
            ps.setInt(1, MWUM.getRecid());
            
            if(ps.executeUpdate() > 0){
                result = true;
                messageResult = "Success delete walkthrough user data.";
            }
        } catch (Exception e) {
            //e.printStackTrace();
            result = false;
            messageResult = ""+e.getMessage();
        }
        
        JSONObject DATA_WALKTHROUGH_USER = new JSONObject();
        
        DATA_WALKTHROUGH_USER.put("RESULT", new Boolean(result));
        DATA_WALKTHROUGH_USER.put("MESSAGE", new String(messageResult));
        DATA_MASTER_WALKTHROUGH_USER.put(DATA_WALKTHROUGH_USER);
            
        JSONObjectRoot.put("DATA_MASTER_WALKTHROUGH_USER", DATA_MASTER_WALKTHROUGH_USER);
        jsonResponse += JSONObjectRoot.toString();
            
        return jsonResponse;
    }
}


