/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author user
 */
public class masterCabangTypeMod {

    /**
     * @return the typeId
     */
    public String getTypeId() {
        return typeId;
    }

    /**
     * @param typeId the typeId to set
     */
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    /**
     * @return the typeName
     */
    public String getTypeName() {
        return typeName;
    }

    /**
     * @param typeName the typeName to set
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * @return the typeBu
     */
    public String getTypeBu() {
        return typeBu;
    }

    /**
     * @param typeBu the typeBu to set
     */
    public void setTypeBu(String typeBu) {
        this.typeBu = typeBu;
    }
    private String typeId;
    private String typeName;
    private String typeBu;
}
