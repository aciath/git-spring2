/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;

/**
 *
 * @author user
 */
public class masterDeviceValidationMod {

    /**
     * @return the recid
     */
    public int getRecid() {
        return recid;
    }

    /**
     * @param recid the recid to set
     */
    public void setRecid(int recid) {
        this.recid = recid;
    }

    /**
     * @return the deviceImei
     */
    public String getDeviceImei() {
        return deviceImei;
    }

    /**
     * @param deviceImei the deviceImei to set
     */
    public void setDeviceImei(String deviceImei) {
        this.deviceImei = deviceImei;
    }

    /**
     * @return the deviceVersionRel
     */
    public String getDeviceVersionRel() {
        return deviceVersionRel;
    }

    /**
     * @param deviceVersionRel the deviceVersionRel to set
     */
    public void setDeviceVersionRel(String deviceVersionRel) {
        this.deviceVersionRel = deviceVersionRel;
    }

    /**
     * @return the deviceVersionSdk
     */
    public String getDeviceVersionSdk() {
        return deviceVersionSdk;
    }

    /**
     * @param deviceVersionSdk the deviceVersionSdk to set
     */
    public void setDeviceVersionSdk(String deviceVersionSdk) {
        this.deviceVersionSdk = deviceVersionSdk;
    }

    /**
     * @return the deviceBoard
     */
    public String getDeviceBoard() {
        return deviceBoard;
    }

    /**
     * @param deviceBoard the deviceBoard to set
     */
    public void setDeviceBoard(String deviceBoard) {
        this.deviceBoard = deviceBoard;
    }

    /**
     * @return the deviceBrand
     */
    public String getDeviceBrand() {
        return deviceBrand;
    }

    /**
     * @param deviceBrand the deviceBrand to set
     */
    public void setDeviceBrand(String deviceBrand) {
        this.deviceBrand = deviceBrand;
    }

    /**
     * @return the deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return the deviceManufacturer
     */
    public String getDeviceManufacturer() {
        return deviceManufacturer;
    }

    /**
     * @param deviceManufacturer the deviceManufacturer to set
     */
    public void setDeviceManufacturer(String deviceManufacturer) {
        this.deviceManufacturer = deviceManufacturer;
    }

    /**
     * @return the deviceModel
     */
    public String getDeviceModel() {
        return deviceModel;
    }

    /**
     * @param deviceModel the deviceModel to set
     */
    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    /**
     * @return the deviceProduct
     */
    public String getDeviceProduct() {
        return deviceProduct;
    }

    /**
     * @param deviceProduct the deviceProduct to set
     */
    public void setDeviceProduct(String deviceProduct) {
        this.deviceProduct = deviceProduct;
    }

    /**
     * @return the deviceSerialNumber
     */
    public String getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    /**
     * @param deviceSerialNumber the deviceSerialNumber to set
     */
    public void setDeviceSerialNumber(String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    /**
     * @return the deviceTime
     */
    public String getDeviceTime() {
        return deviceTime;
    }

    /**
     * @param deviceTime the deviceTime to set
     */
    public void setDeviceTime(String deviceTime) {
        this.deviceTime = deviceTime;
    }

    /**
     * @return the deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * @param deviceType the deviceType to set
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * @return the deviceUser
     */
    public String getDeviceUser() {
        return deviceUser;
    }

    /**
     * @param deviceUser the deviceUser to set
     */
    public void setDeviceUser(String deviceUser) {
        this.deviceUser = deviceUser;
    }

    /**
     * @return the deviceGPSLatitude
     */
    public String getDeviceGPSLatitude() {
        return deviceGPSLatitude;
    }

    /**
     * @param deviceGPSLatitude the deviceGPSLatitude to set
     */
    public void setDeviceGPSLatitude(String deviceGPSLatitude) {
        this.deviceGPSLatitude = deviceGPSLatitude;
    }

    /**
     * @return the deviceGPSLongitude
     */
    public String getDeviceGPSLongitude() {
        return deviceGPSLongitude;
    }

    /**
     * @param deviceGPSLongitude the deviceGPSLongitude to set
     */
    public void setDeviceGPSLongitude(String deviceGPSLongitude) {
        this.deviceGPSLongitude = deviceGPSLongitude;
    }

    /**
     * @return the deviceVerificationKey
     */
    public String getDeviceVerificationKey() {
        return deviceVerificationKey;
    }

    /**
     * @param deviceVerificationKey the deviceVerificationKey to set
     */
    public void setDeviceVerificationKey(String deviceVerificationKey) {
        this.deviceVerificationKey = deviceVerificationKey;
    }

    /**
     * @return the deviceVerificationDate
     */
    public String getDeviceVerificationDate() {
        return deviceVerificationDate;
    }

    /**
     * @param deviceVerificationDate the deviceVerificationDate to set
     */
    public void setDeviceVerificationDate(String deviceVerificationDate) {
        this.deviceVerificationDate = deviceVerificationDate;
    }

    /**
     * @return the isActive
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * @param isActive the isActive to set
     */
    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    /**
     * @return the createdOpt
     */
    public String getCreatedOpt() {
        return createdOpt;
    }

    /**
     * @param createdOpt the createdOpt to set
     */
    public void setCreatedOpt(String createdOpt) {
        this.createdOpt = createdOpt;
    }

    /**
     * @return the createdDT
     */
    public String getCreatedDT() {
        return createdDT;
    }

    /**
     * @param createdDT the createdDT to set
     */
    public void setCreatedDT(String createdDT) {
        this.createdDT = createdDT;
    }

    /**
     * @return the editedOpt
     */
    public String getEditedOpt() {
        return editedOpt;
    }

    /**
     * @param editedOpt the editedOpt to set
     */
    public void setEditedOpt(String editedOpt) {
        this.editedOpt = editedOpt;
    }

    /**
     * @return the editedDT
     */
    public String getEditedDT() {
        return editedDT;
    }

    /**
     * @param editedDT the editedDT to set
     */
    public void setEditedDT(String editedDT) {
        this.editedDT = editedDT;
    }

    /**
     * @return the clientAddr
     */
    public String getClientAddr() {
        return clientAddr;
    }

    /**
     * @param clientAddr the clientAddr to set
     */
    public void setClientAddr(String clientAddr) {
        this.clientAddr = clientAddr;
    }
    private int recid;
    private String deviceImei;
    private String deviceVersionRel;
    private String deviceVersionSdk;
    private String deviceBoard;
    private String deviceBrand;
    private String deviceId;
    private String deviceManufacturer;
    private String deviceModel;
    private String deviceProduct;
    private String deviceSerialNumber;
    private String deviceTime;
    private String deviceType;
    private String deviceUser;
    private String deviceGPSLatitude;
    private String deviceGPSLongitude;
    private String deviceVerificationKey;
    private String deviceVerificationDate;
    private String isActive;
    private String createdOpt;
    private String createdDT;
    private String editedOpt;
    private String editedDT;
    private String clientAddr;
}
