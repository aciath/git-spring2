/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author user
 */
public class masterWalkthroughActivitiesMod {

    /**
     * @return the activitiesBu
     */
    public String getActivitiesBu() {
        return activitiesBu;
    }

    /**
     * @param activitiesBu the activitiesBu to set
     */
    public void setActivitiesBu(String activitiesBu) {
        this.activitiesBu = activitiesBu;
    }

    /**
     * @return the activitiesDiv
     */
    public String getActivitiesDiv() {
        return activitiesDiv;
    }

    /**
     * @param activitiesDiv the activitiesDiv to set
     */
    public void setActivitiesDiv(String activitiesDiv) {
        this.activitiesDiv = activitiesDiv;
    }

    /**
     * @return the activitiesName
     */
    public String getActivitiesName() {
        return activitiesName;
    }

    /**
     * @param activitiesName the activitiesName to set
     */
    public void setActivitiesName(String activitiesName) {
        this.activitiesName = activitiesName;
    }

    /**
     * @return the activitiesTimeStart
     */
    public String getActivitiesTimeStart() {
        return activitiesTimeStart;
    }

    /**
     * @param activitiesTimeStart the activitiesTimeStart to set
     */
    public void setActivitiesTimeStart(String activitiesTimeStart) {
        this.activitiesTimeStart = activitiesTimeStart;
    }

    /**
     * @return the activitiesTimeEnd
     */
    public String getActivitiesTimeEnd() {
        return activitiesTimeEnd;
    }

    /**
     * @param activitiesTimeEnd the activitiesTimeEnd to set
     */
    public void setActivitiesTimeEnd(String activitiesTimeEnd) {
        this.activitiesTimeEnd = activitiesTimeEnd;
    }

    /**
     * @return the createdOpt
     */
    public String getCreatedOpt() {
        return createdOpt;
    }

    /**
     * @param createdOpt the createdOpt to set
     */
    public void setCreatedOpt(String createdOpt) {
        this.createdOpt = createdOpt;
    }

    /**
     * @return the createdDt
     */
    public String getCreatedDt() {
        return createdDt;
    }

    /**
     * @param createdDt the createdDt to set
     */
    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

    /**
     * @return the editedOpt
     */
    public String getEditedOpt() {
        return editedOpt;
    }

    /**
     * @param editedOpt the editedOpt to set
     */
    public void setEditedOpt(String editedOpt) {
        this.editedOpt = editedOpt;
    }

    /**
     * @return the editedDt
     */
    public String getEditedDt() {
        return editedDt;
    }

    /**
     * @param editedDt the editedDt to set
     */
    public void setEditedDt(String editedDt) {
        this.editedDt = editedDt;
    }

    /**
     * @return the clientAddr
     */
    public String getClientAddr() {
        return clientAddr;
    }

    /**
     * @param clientAddr the clientAddr to set
     */
    public void setClientAddr(String clientAddr) {
        this.clientAddr = clientAddr;
    }
    private String activitiesBu;
    private String activitiesDiv;
    private String activitiesName;
    private String activitiesTimeStart;
    private String activitiesTimeEnd;
    private String createdOpt;
    private String createdDt;
    private String editedOpt;
    private String editedDt;
    private String clientAddr;
}
