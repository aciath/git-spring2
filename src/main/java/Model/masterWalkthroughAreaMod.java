/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author user
 */
public class masterWalkthroughAreaMod {

    /**
     * @return the recid
     */
    public int getRecid() {
        return recid;
    }

    /**
     * @param recid the recid to set
     */
    public void setRecid(int recid) {
        this.recid = recid;
    }

    /**
     * @return the areaBu
     */
    public String getAreaBu() {
        return areaBu;
    }

    /**
     * @param areaBu the areaBu to set
     */
    public void setAreaBu(String areaBu) {
        this.areaBu = areaBu;
    }

    /**
     * @return the areaDiv
     */
    public String getAreaDiv() {
        return areaDiv;
    }

    /**
     * @param areaDiv the areaDiv to set
     */
    public void setAreaDiv(String areaDiv) {
        this.areaDiv = areaDiv;
    }

    /**
     * @return the wilayah
     */
    public String getWilayah() {
        return wilayah;
    }

    /**
     * @param wilayah the wilayah to set
     */
    public void setWilayah(String wilayah) {
        this.wilayah = wilayah;
    }

    /**
     * @return the lokasi
     */
    public String getLokasi() {
        return lokasi;
    }

    /**
     * @param lokasi the lokasi to set
     */
    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    /**
     * @return the isfstandOnly
     */
    public String getIsfstandOnly() {
        return isfstandOnly;
    }

    /**
     * @param isfstandOnly the isfstandOnly to set
     */
    public void setIsfstandOnly(String isfstandOnly) {
        this.isfstandOnly = isfstandOnly;
    }
    private int recid;
    private String areaBu;
    private String areaDiv;
    private String wilayah;
    private String lokasi;
    private String isfstandOnly;
}
