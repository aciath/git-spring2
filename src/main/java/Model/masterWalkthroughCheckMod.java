/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author user
 */
public class masterWalkthroughCheckMod {

    /**
     * @return the checkNotes
     */
    public String getCheckNotes() {
        return checkNotes;
    }

    /**
     * @param checkNotes the checkNotes to set
     */
    public void setCheckNotes(String checkNotes) {
        this.checkNotes = checkNotes;
    }

    /**
     * @return the recid
     */
    public int getRecid() {
        return recid;
    }

    /**
     * @param recid the recid to set
     */
    public void setRecid(int recid) {
        this.recid = recid;
    }

    /**
     * @return the checkType
     */
    public String getCheckType() {
        return checkType;
    }

    /**
     * @param checkType the checkType to set
     */
    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the cabangId
     */
    public String getCabangId() {
        return cabangId;
    }

    /**
     * @param cabangId the cabangId to set
     */
    public void setCabangId(String cabangId) {
        this.cabangId = cabangId;
    }

    /**
     * @return the checkDt
     */
    public String getCheckDt() {
        return checkDt;
    }

    /**
     * @param checkDt the checkDt to set
     */
    public void setCheckDt(String checkDt) {
        this.checkDt = checkDt;
    }

    /**
     * @return the checkId
     */
    public int getCheckId() {
        return checkId;
    }

    /**
     * @param checkId the checkId to set
     */
    public void setCheckId(int checkId) {
        this.checkId = checkId;
    }

    /**
     * @return the checkResult
     */
    public String getCheckResult() {
        return checkResult;
    }

    /**
     * @param checkResult the checkResult to set
     */
    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

    /**
     * @return the checkPoin
     */
    public int getCheckPoin() {
        return checkPoin;
    }

    /**
     * @param checkPoin the checkPoin to set
     */
    public void setCheckPoin(int checkPoin) {
        this.checkPoin = checkPoin;
    }

    /**
     * @return the checkStat
     */
    public int getCheckStat() {
        return checkStat;
    }

    /**
     * @param checkStat the checkStat to set
     */
    public void setCheckStat(int checkStat) {
        this.checkStat = checkStat;
    }

    /**
     * @return the deviceId
     */
    public int getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId the deviceId to set
     */
    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return the createdOpt
     */
    public String getCreatedOpt() {
        return createdOpt;
    }

    /**
     * @param createdOpt the createdOpt to set
     */
    public void setCreatedOpt(String createdOpt) {
        this.createdOpt = createdOpt;
    }

    /**
     * @return the createdDt
     */
    public String getCreatedDt() {
        return createdDt;
    }

    /**
     * @param createdDt the createdDt to set
     */
    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

    /**
     * @return the editedOpt
     */
    public String getEditedOpt() {
        return editedOpt;
    }

    /**
     * @param editedOpt the editedOpt to set
     */
    public void setEditedOpt(String editedOpt) {
        this.editedOpt = editedOpt;
    }

    /**
     * @return the editedDt
     */
    public String getEditedDt() {
        return editedDt;
    }

    /**
     * @param editedDt the editedDt to set
     */
    public void setEditedDt(String editedDt) {
        this.editedDt = editedDt;
    }

    /**
     * @return the clientAddr
     */
    public String getClientAddr() {
        return clientAddr;
    }

    /**
     * @param clientAddr the clientAddr to set
     */
    public void setClientAddr(String clientAddr) {
        this.clientAddr = clientAddr;
    }
    private int recid;
    private String checkType;
    private String userId;
    private String cabangId;
    private String checkDt;
    private int checkId;
    private String checkResult;
    private int checkPoin;
    private String checkNotes;
    private int checkStat;
    private int deviceId;
    private String createdOpt;
    private String createdDt;
    private String editedOpt;
    private String editedDt;
    private String clientAddr;
}
