/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author user
 */
public class masterWalkthroughChecklistMod {

    /**
     * @return the recid
     */
    public int getRecid() {
        return recid;
    }

    /**
     * @param recid the recid to set
     */
    public void setRecid(int recid) {
        this.recid = recid;
    }

    /**
     * @return the chkObjectId
     */
    public int getChkObjectId() {
        return chkObjectId;
    }

    /**
     * @param chkObjectId the chkObjectId to set
     */
    public void setChkObjectId(int chkObjectId) {
        this.chkObjectId = chkObjectId;
    }

    /**
     * @return the chkObjectUrut
     */
    public int getChkObjectUrut() {
        return chkObjectUrut;
    }

    /**
     * @param chkObjectUrut the chkObjectUrut to set
     */
    public void setChkObjectUrut(int chkObjectUrut) {
        this.chkObjectUrut = chkObjectUrut;
    }

    /**
     * @return the chkObjectDetail
     */
    public String getChkObjectDetail() {
        return chkObjectDetail;
    }

    /**
     * @param chkObjectDetail the chkObjectDetail to set
     */
    public void setChkObjectDetail(String chkObjectDetail) {
        this.chkObjectDetail = chkObjectDetail;
    }

    /**
     * @return the chkObjectDesc
     */
    public String getChkObjectDesc() {
        return chkObjectDesc;
    }

    /**
     * @param chkObjectDesc the chkObjectDesc to set
     */
    public void setChkObjectDesc(String chkObjectDesc) {
        this.chkObjectDesc = chkObjectDesc;
    }

    /**
     * @return the chkObjectType
     */
    public String getChkObjectType() {
        return chkObjectType;
    }

    /**
     * @param chkObjectType the chkObjectType to set
     */
    public void setChkObjectType(String chkObjectType) {
        this.chkObjectType = chkObjectType;
    }

    /**
     * @return the chkObjectNilai
     */
    public int getChkObjectNilai() {
        return chkObjectNilai;
    }

    /**
     * @param chkObjectNilai the chkObjectNilai to set
     */
    public void setChkObjectNilai(int chkObjectNilai) {
        this.chkObjectNilai = chkObjectNilai;
    }

    /**
     * @return the ischkObjectWt
     */
    public String getIschkObjectWt() {
        return ischkObjectWt;
    }

    /**
     * @param ischkObjectWt the ischkObjectWt to set
     */
    public void setIschkObjectWt(String ischkObjectWt) {
        this.ischkObjectWt = ischkObjectWt;
    }

    /**
     * @return the ischkObjectSa
     */
    public String getIschkObjectSa() {
        return ischkObjectSa;
    }

    /**
     * @param ischkObjectSa the ischkObjectSa to set
     */
    public void setIschkObjectSa(String ischkObjectSa) {
        this.ischkObjectSa = ischkObjectSa;
    }

    /**
     * @return the ischkObjectPict
     */
    public String getIschkObjectPict() {
        return ischkObjectPict;
    }

    /**
     * @param ischkObjectPict the ischkObjectPict to set
     */
    public void setIschkObjectPict(String ischkObjectPict) {
        this.ischkObjectPict = ischkObjectPict;
    }

    /**
     * @return the isactive
     */
    public String getIsactive() {
        return isactive;
    }

    /**
     * @param isactive the isactive to set
     */
    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    /**
     * @return the createdOpt
     */
    public String getCreatedOpt() {
        return createdOpt;
    }

    /**
     * @param createdOpt the createdOpt to set
     */
    public void setCreatedOpt(String createdOpt) {
        this.createdOpt = createdOpt;
    }

    /**
     * @return the createdDt
     */
    public String getCreatedDt() {
        return createdDt;
    }

    /**
     * @param createdDt the createdDt to set
     */
    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

    /**
     * @return the editedOpt
     */
    public String getEditedOpt() {
        return editedOpt;
    }

    /**
     * @param editedOpt the editedOpt to set
     */
    public void setEditedOpt(String editedOpt) {
        this.editedOpt = editedOpt;
    }

    /**
     * @return the editedDt
     */
    public String getEditedDt() {
        return editedDt;
    }

    /**
     * @param editedDt the editedDt to set
     */
    public void setEditedDt(String editedDt) {
        this.editedDt = editedDt;
    }

    /**
     * @return the clientAddr
     */
    public String getClientAddr() {
        return clientAddr;
    }

    /**
     * @param clientAddr the clientAddr to set
     */
    public void setClientAddr(String clientAddr) {
        this.clientAddr = clientAddr;
    }
    private int recid;
    private int chkObjectId;
    private int chkObjectUrut;
    private String chkObjectDetail;
    private String chkObjectDesc;
    private String chkObjectType;
    private int chkObjectNilai;
    private String ischkObjectWt;
    private String ischkObjectSa;
    private String ischkObjectPict;
    private String isactive;
    private String createdOpt;
    private String createdDt;
    private String editedOpt;
    private String editedDt;
    private String clientAddr;
}
