/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author user
 */
public class masterWalkthroughObjectMod {

    /**
     * @return the objPic
     */
    public String getObjPic() {
        return objPic;
    }

    /**
     * @param objPic the objPic to set
     */
    public void setObjPic(String objPic) {
        this.objPic = objPic;
    }

    /**
     * @return the recid
     */
    public int getRecid() {
        return recid;
    }

    /**
     * @param recid the recid to set
     */
    public void setRecid(int recid) {
        this.recid = recid;
    }

    /**
     * @return the objBu
     */
    public String getObjBu() {
        return objBu;
    }

    /**
     * @param objBu the objBu to set
     */
    public void setObjBu(String objBu) {
        this.objBu = objBu;
    }

    /**
     * @return the objDiv
     */
    public String getObjDiv() {
        return objDiv;
    }

    /**
     * @param objDiv the objDiv to set
     */
    public void setObjDiv(String objDiv) {
        this.objDiv = objDiv;
    }

    /**
     * @return the objActivities
     */
    public String getObjActivities() {
        return objActivities;
    }

    /**
     * @param objActivities the objActivities to set
     */
    public void setObjActivities(String objActivities) {
        this.objActivities = objActivities;
    }

    /**
     * @return the objWilayah
     */
    public String getObjWilayah() {
        return objWilayah;
    }

    /**
     * @param objWilayah the objWilayah to set
     */
    public void setObjWilayah(String objWilayah) {
        this.objWilayah = objWilayah;
    }

    /**
     * @return the objLokasi
     */
    public String getObjLokasi() {
        return objLokasi;
    }

    /**
     * @param objLokasi the objLokasi to set
     */
    public void setObjLokasi(String objLokasi) {
        this.objLokasi = objLokasi;
    }

    /**
     * @return the objName
     */
    public String getObjName() {
        return objName;
    }

    /**
     * @param objName the objName to set
     */
    public void setObjName(String objName) {
        this.objName = objName;
    }

    /**
     * @return the objUrut
     */
    public int getObjUrut() {
        return objUrut;
    }

    /**
     * @param objUrut the objUrut to set
     */
    public void setObjUrut(int objUrut) {
        this.objUrut = objUrut;
    }

    /**
     * @return the createdOpt
     */
    public String getCreatedOpt() {
        return createdOpt;
    }

    /**
     * @param createdOpt the createdOpt to set
     */
    public void setCreatedOpt(String createdOpt) {
        this.createdOpt = createdOpt;
    }

    /**
     * @return the createdDt
     */
    public String getCreatedDt() {
        return createdDt;
    }

    /**
     * @param createdDt the createdDt to set
     */
    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

    /**
     * @return the editedOpt
     */
    public String getEditedOpt() {
        return editedOpt;
    }

    /**
     * @param editedOpt the editedOpt to set
     */
    public void setEditedOpt(String editedOpt) {
        this.editedOpt = editedOpt;
    }

    /**
     * @return the editedDt
     */
    public String getEditedDt() {
        return editedDt;
    }

    /**
     * @param editedDt the editedDt to set
     */
    public void setEditedDt(String editedDt) {
        this.editedDt = editedDt;
    }

    /**
     * @return the clientAddr
     */
    public String getClientAddr() {
        return clientAddr;
    }

    /**
     * @param clientAddr the clientAddr to set
     */
    public void setClientAddr(String clientAddr) {
        this.clientAddr = clientAddr;
    }
    private int recid;
    private String objBu;
    private String objDiv;
    private String objActivities;
    private String objWilayah;
    private String objLokasi;
    private String objName;
    private int objUrut;
    private String objPic;
    private String createdOpt;
    private String createdDt;
    private String editedOpt;
    private String editedDt;
    private String clientAddr;
}
