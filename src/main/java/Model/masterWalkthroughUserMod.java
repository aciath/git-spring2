/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author user
 */
public class masterWalkthroughUserMod {

    /**
     * @return the recid
     */
    public int getRecid() {
        return recid;
    }

    /**
     * @param recid the recid to set
     */
    public void setRecid(int recid) {
        this.recid = recid;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the userDiv
     */
    public String getUserDiv() {
        return userDiv;
    }

    /**
     * @param userDiv the userDiv to set
     */
    public void setUserDiv(String userDiv) {
        this.userDiv = userDiv;
    }

    /**
     * @return the userArea
     */
    public String getUserArea() {
        return userArea;
    }

    /**
     * @param userArea the userArea to set
     */
    public void setUserArea(String userArea) {
        this.userArea = userArea;
    }

    /**
     * @return the isallowWt
     */
    public String getIsallowWt() {
        return isallowWt;
    }

    /**
     * @param isallowWt the isallowWt to set
     */
    public void setIsallowWt(String isallowWt) {
        this.isallowWt = isallowWt;
    }

    /**
     * @return the isallowSa
     */
    public String getIsallowSa() {
        return isallowSa;
    }

    /**
     * @param isallowSa the isallowSa to set
     */
    public void setIsallowSa(String isallowSa) {
        this.isallowSa = isallowSa;
    }

    /**
     * @return the createdOpt
     */
    public String getCreatedOpt() {
        return createdOpt;
    }

    /**
     * @param createdOpt the createdOpt to set
     */
    public void setCreatedOpt(String createdOpt) {
        this.createdOpt = createdOpt;
    }

    /**
     * @return the createdDt
     */
    public String getCreatedDt() {
        return createdDt;
    }

    /**
     * @param createdDt the createdDt to set
     */
    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

    /**
     * @return the editedOpt
     */
    public String getEditedOpt() {
        return editedOpt;
    }

    /**
     * @param editedOpt the editedOpt to set
     */
    public void setEditedOpt(String editedOpt) {
        this.editedOpt = editedOpt;
    }

    /**
     * @return the editedDt
     */
    public String getEditedDt() {
        return editedDt;
    }

    /**
     * @param editedDt the editedDt to set
     */
    public void setEditedDt(String editedDt) {
        this.editedDt = editedDt;
    }

    /**
     * @return the clientAddr
     */
    public String getClientAddr() {
        return clientAddr;
    }

    /**
     * @param clientAddr the clientAddr to set
     */
    public void setClientAddr(String clientAddr) {
        this.clientAddr = clientAddr;
    }
    private int recid;
    private String userId;
    private String userDiv;
    private String userArea;
    private String isallowWt;
    private String isallowSa;
    private String createdOpt;
    private String createdDt;
    private String editedOpt;
    private String editedDt;
    private String clientAddr;
}
