/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author user
 */
public class walkthroughDPMod_schedulerList {

    private int recidWTDPChecklist;
    private String Aktivitas;
    private String Devisi;
    private String Obyek;
    private String Detail;
    private String Deskripsi;
    private String Photo;
    private String isPhoto;
    private String jamStart;
    private String jamEnd;
    /**
     * @return the recidWTDPChecklist
     */
    public int getRecidWTDPChecklist() {
        return recidWTDPChecklist;
    }

    /**
     * @param recidWTDPChecklist the recidWTDPChecklist to set
     */
    public void setRecidWTDPChecklist(int recidWTDPChecklist) {
        this.recidWTDPChecklist = recidWTDPChecklist;
    }

    /**
     * @return the Aktivitas
     */
    public String getAktivitas() {
        return Aktivitas;
    }

    /**
     * @param Aktivitas the Aktivitas to set
     */
    public void setAktivitas(String Aktivitas) {
        this.Aktivitas = Aktivitas;
    }

    /**
     * @return the Devisi
     */
    public String getDevisi() {
        return Devisi;
    }

    /**
     * @param Devisi the Devisi to set
     */
    public void setDevisi(String Devisi) {
        this.Devisi = Devisi;
    }

    /**
     * @return the Obyek
     */
    public String getObyek() {
        return Obyek;
    }

    /**
     * @param Obyek the Obyek to set
     */
    public void setObyek(String Obyek) {
        this.Obyek = Obyek;
    }

    /**
     * @return the Detail
     */
    public String getDetail() {
        return Detail;
    }

    /**
     * @param Detail the Detail to set
     */
    public void setDetail(String Detail) {
        this.Detail = Detail;
    }

    /**
     * @return the Deskripsi
     */
    public String getDeskripsi() {
        return Deskripsi;
    }

    /**
     * @param Deskripsi the Deskripsi to set
     */
    public void setDeskripsi(String Deskripsi) {
        this.Deskripsi = Deskripsi;
    }

    /**
     * @return the Photo
     */
    public String getPhoto() {
        return Photo;
    }

    /**
     * @param Photo the Photo to set
     */
    public void setPhoto(String Photo) {
        this.Photo = Photo;
    }

    /**
     * @return the isPhoto
     */
    public String getIsPhoto() {
        return isPhoto;
    }

    /**
     * @param isPhoto the isPhoto to set
     */
    public void setIsPhoto(String isPhoto) {
        this.isPhoto = isPhoto;
    }

    /**
     * @return the jamStart
     */
    public String getJamStart() {
        return jamStart;
    }

    /**
     * @param jamStart the jamStart to set
     */
    public void setJamStart(String jamStart) {
        this.jamStart = jamStart;
    }

    /**
     * @return the jamEnd
     */
    public String getJamEnd() {
        return jamEnd;
    }

    /**
     * @param jamEnd the jamEnd to set
     */
    public void setJamEnd(String jamEnd) {
        this.jamEnd = jamEnd;
    }
    
}
