/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverService;

import DAO.masterDeviceValidationDAO;
import Model.masterDeviceValidationMod;
import Model.responseInfoServices;
import dbConnection.dbConnection;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import javax.servlet.ServletContext;
import javax.validation.Valid;
//import javax.ws.rs.core.MultivaluedMap;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author user
 */
@RestController
@RequestMapping(value="/devicevalidation")
public class masterDeviceValidation {
    @Autowired
    ServletContext context;
    private Connection conn = null;
    
    // [GET] web server services untuk mengambil seluruh data master cabang dari mobile apps ke web services, mapping : http://ipserver:8080/cabang/getallcabang
    @CrossOrigin
    @RequestMapping(value="/getalldevicevalidation", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public String getAllDeviceValidationResources() {
        String All = "";
        masterDeviceValidationDAO MDVDAO = new masterDeviceValidationDAO();
        All = MDVDAO.getAllDeviceValidation();
        return All;
    }
    
    @CrossOrigin
    @RequestMapping(value="/getspecificdevicevalidation", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> getValidationDeviceResources(@Valid @RequestParam("typeId") String recid) {
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterDeviceValidationMod MDVM = new masterDeviceValidationMod();
                MDVM.setRecid(Integer.parseInt(recid)); // ini adalah integer, bisa diubah sesuai dengan isi recid nanti di database
                
                masterDeviceValidationDAO MDVDAO = new masterDeviceValidationDAO();
                String jsonResponse = MDVDAO.getDeviceValidation(MDVM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/addnewdevicevalidation", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> addDeviceValidationResources(
            @RequestParam("deviceImei") String deviceImei,
            @RequestParam("deviceModel") String deviceModel,
            @RequestParam("deviceProduct") String deviceProduct,
            @RequestParam("deviceSerialNumber") String deviceSerialNumber,
            @RequestParam("createdOpt") String createdOpt) {
        
        // automatically add : created_opt, created_dt, edited_opt, edited_dt, client_address
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!deviceImei.equals("")) {
            try {
                masterDeviceValidationMod MDVM = new masterDeviceValidationMod();
                MDVM.setDeviceImei(deviceImei);
//                MDVM.setDeviceVersionRel(deviceVersionRel);
//                MDVM.setDeviceVersionSdk(deviceVersionSdk);
//                MDVM.setDeviceBoard(deviceBoard);
//                MDVM.setDeviceBrand(deviceBrand);
//                MDVM.setDeviceId(deviceId);
//                MDVM.setDeviceManufacturer(deviceManufacturer);
                MDVM.setDeviceModel(deviceModel);
                MDVM.setDeviceProduct(deviceProduct);
                MDVM.setDeviceSerialNumber(deviceSerialNumber);
//                MDVM.setDeviceTime(deviceTime);
//                MDVM.setDeviceType(deviceType);
//                MDVM.setDeviceUser(deviceUser);
//                MDVM.setDeviceGPSLatitude(deviceGpsLatitude);
//                MDVM.setDeviceGPSLongitude(deviceGpsLongtiude);
//                MDVM.setDeviceVerificationKey(deviceVerificationKey);
//                MDVM.setDeviceVerificationDate(deviceVerificationDate);
//                MDVM.setIsActive(isactive);
                MDVM.setCreatedOpt(createdOpt);
                
                masterDeviceValidationDAO MDVDAO = new masterDeviceValidationDAO();
                String jsonResponse = MDVDAO.saveMasterDeviceValidation(MDVM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/editdevicevalidation", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> editDeviceValidationResources(
            @RequestParam("recid") String recid,
            @RequestParam("deviceImei") String deviceImei,
            @RequestParam("deviceModel") String deviceModel,
            @RequestParam("deviceProduct") String deviceProduct,
            @RequestParam("deviceSerialNumber") String deviceSerialNumber,
            @RequestParam("createdOpt") String createdOpt,
            @RequestParam("createdDt") String createdDt,
            @RequestParam("editedOpt") String editedOpt) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterDeviceValidationMod MDVM = new masterDeviceValidationMod();
                MDVM.setRecid(Integer.parseInt(recid));
                MDVM.setDeviceImei(deviceImei);
//                MDVM.setDeviceVersionRel(deviceVersionRel);
//                MDVM.setDeviceVersionSdk(deviceVersionSdk);
//                MDVM.setDeviceBoard(deviceBoard);
//                MDVM.setDeviceBrand(deviceBrand);
//                MDVM.setDeviceId(deviceId);
//                MDVM.setDeviceManufacturer(deviceManufacturer);
                MDVM.setDeviceModel(deviceModel);
                MDVM.setDeviceProduct(deviceProduct);
                MDVM.setDeviceSerialNumber(deviceSerialNumber);
//                MDVM.setDeviceTime(deviceTime);
//                MDVM.setDeviceType(deviceType);
//                MDVM.setDeviceUser(deviceUser);
//                MDVM.setDeviceGPSLatitude(deviceGpsLatitude);
//                MDVM.setDeviceGPSLongitude(deviceGpsLongtiude);
//                MDVM.setDeviceVerificationKey(deviceVerificationKey);
//                MDVM.setDeviceVerificationDate(deviceVerificationDate);
//                MDVM.setIsActive(isactive);
                MDVM.setCreatedOpt(createdOpt);
                MDVM.setCreatedDT(createdDt);
                MDVM.setEditedOpt(editedOpt);
                
                masterDeviceValidationDAO MDVDAO = new masterDeviceValidationDAO();
                String jsonResponse = MDVDAO.updateMasterDeviceValidation(MDVM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/deletedevicevalidation", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> deleteDeviceValidationResources(
            @RequestParam("deviceImei") String deviceImei,
            @RequestParam("deviceSerialNumber") String deviceSerialNumber) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!deviceImei.equals("") && !deviceImei.equals("")) {
            try {
                masterDeviceValidationMod MDVM = new masterDeviceValidationMod();
                MDVM.setDeviceImei(deviceImei);
                MDVM.setDeviceSerialNumber(deviceSerialNumber);
                
                masterDeviceValidationDAO MDVDAO = new masterDeviceValidationDAO();
                String jsonResponse = MDVDAO.deleteMasterDeviceValidation(MDVM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    /*@CrossOrigin
    @RequestMapping(value="/validatedevice", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> validateDevice(final MultivaluedMap<String, String> formParams) throws JSONException {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!formParams.equals(null)) {
            try {
                masterDeviceValidationMod MDVM = new masterDeviceValidationMod();
                MDVM.setDeviceImei(formParams.getFirst("IMEI"));
                MDVM.setDeviceVersionRel(formParams.getFirst("VERSION.RELEASE"));
                MDVM.setDeviceVersionSdk(formParams.getFirst("VERSION.SDK.NUMBER"));
                MDVM.setDeviceBoard(formParams.getFirst("BOARD"));
                MDVM.setDeviceBrand(formParams.getFirst("BRAND"));
                MDVM.setDeviceId(formParams.getFirst("ID"));
                MDVM.setDeviceManufacturer(formParams.getFirst("MANUFACTURER"));
                MDVM.setDeviceModel(formParams.getFirst("MODEL"));
                MDVM.setDeviceProduct(formParams.getFirst("PRODUCT"));
                MDVM.setDeviceSerialNumber(formParams.getFirst("SERIAL"));
                MDVM.setDeviceTime(formParams.getFirst("TIME"));
                MDVM.setDeviceType(formParams.getFirst("TYPE"));
                MDVM.setDeviceUser(formParams.getFirst("USER"));
                //MDVM.setDeviceGPSLatitude(formParams.getFirst(""));
                //MDVM.setDeviceGPSLongitude(formParams.getFirst(""));
                //MDVM.setDeviceVerificationKey(formParams.getFirst(""));
                //MDVM.setDeviceVerificationDate(formParams.getFirst(""));
                //MDVM.setIsActive(formParams.getFirst(""));
                
                masterDeviceValidationDAO MDVDAO = new masterDeviceValidationDAO();
                String jsonResponse = MDVDAO.validatingDevice(MDVM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }*/
}
