/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverService;


import DAO.masterWalkthroughCheckDAO;
import Model.masterWalkthroughCheckMod;
import Model.responseInfoServices;
import java.sql.Connection;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author user
 */
@RestController
@RequestMapping(value="/check")
public class masterWalkthroughCheck {
        @Autowired
    ServletContext context;
    private Connection conn = null;
    
    // [GET] web server services untuk mengambil seluruh data master cabang dari mobile apps ke web services, mapping : http://ipserver:8080/cabang/getallcabang
    @CrossOrigin
    @RequestMapping(value="/getallcheck", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public String getAllWalkthroughMasterCheckResources() {
        String All = "";
        masterWalkthroughCheckDAO MWCDAO = new masterWalkthroughCheckDAO();
        All = MWCDAO.getAllMasterWalkthroughCheck();
        return All;
    }
    
    @CrossOrigin
    @RequestMapping(value="/getspecificcheck", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> getSpecificWalkthroughMasterCheckResources(@Valid @RequestParam("recid") String recid) {
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterWalkthroughCheckMod MWCM = new masterWalkthroughCheckMod();
                MWCM.setRecid(Integer.parseInt(recid)); // ini adalah integer, bisa diubah sesuai dengan isi recid nanti di database
                
                masterWalkthroughCheckDAO MWCDAO = new masterWalkthroughCheckDAO();
                String jsonResponse = MWCDAO.getMasterWalkthroughCheck(MWCM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/addcheck", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> addWalkthroughMasterCheckResources(
            @RequestParam("addCheckType") String checkType,
            @RequestParam("addCheckUserId") String checkUserId,
            @RequestParam("addCheckCabangId") String checkCabangId,
            @RequestParam("addCheckDt") String checkDt,
            @RequestParam("addCheckId") String checkId,
            @RequestParam("addCheckResult") String checkResult,
            @RequestParam("addCheckPoin") String checkPoin,
            @RequestParam("addCheckNotes") String checkNotes,
            @RequestParam("addCheckStat") String checkStat,
            @RequestParam("addCheckDeviceId") String deviceId,
            @RequestParam("addCreatedOpt") String createdOpt) {
        
        // automatically add : created_opt, created_dt, edited_opt, edited_dt, client_address
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!checkType.equals("")) {
            try {
                masterWalkthroughCheckMod MWCM = new masterWalkthroughCheckMod();
                MWCM.setCheckType(checkType);
                MWCM.setUserId(checkUserId);
                MWCM.setCabangId(checkCabangId);
                MWCM.setCheckDt(checkDt);
                MWCM.setCheckId(Integer.parseInt(checkId));
                MWCM.setCheckResult(checkResult);
                MWCM.setCheckPoin(Integer.parseInt(checkPoin));
                MWCM.setCheckNotes(checkNotes);
                MWCM.setCheckStat(Integer.parseInt(checkStat));
                MWCM.setDeviceId(Integer.parseInt(deviceId));
                MWCM.setCreatedOpt(createdOpt);
                
                masterWalkthroughCheckDAO MWCDAO = new masterWalkthroughCheckDAO();
                String jsonResponse = MWCDAO.saveMasterWalkthroughCheck(MWCM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/checkobject", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> editWalkthroughMasterCheckResources(
            @RequestParam("addCheckType") String checkType,
            @RequestParam("addCheckUserId") String checkUserId,
            @RequestParam("addCheckCabangId") String checkCabangId,
            @RequestParam("addCheckDt") String checkDt,
            @RequestParam("addCheckId") String checkId,
            @RequestParam("addCheckResult") String checkResult,
            @RequestParam("addCheckPoin") String checkPoin,
            @RequestParam("addCheckNotes") String checkNotes,
            @RequestParam("addCheckStat") String checkStat,
            @RequestParam("addCheckDeviceId") String deviceId,
            @RequestParam("editEditedOpt") String editedOpt,
            @RequestParam("editRecid") String recid) {
        
        // automatically add : created_opt, created_dt, edited_opt, edited_dt, client_address
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("") && !checkUserId.equals("")) {
            try {
                masterWalkthroughCheckMod MWCM = new masterWalkthroughCheckMod();
                MWCM.setCheckType(checkType);
                MWCM.setUserId(checkUserId);
                MWCM.setCabangId(checkCabangId);
                MWCM.setCheckDt(checkDt);
                MWCM.setCheckId(Integer.parseInt(checkId));
                MWCM.setCheckResult(checkResult);
                MWCM.setCheckPoin(Integer.parseInt(checkPoin));
                MWCM.setCheckNotes(checkNotes);
                MWCM.setCheckStat(Integer.parseInt(checkStat));
                MWCM.setDeviceId(Integer.parseInt(deviceId));
                MWCM.setEditedOpt(editedOpt);
                
                masterWalkthroughCheckDAO MWCDAO = new masterWalkthroughCheckDAO();
                String jsonResponse = MWCDAO.updateMasterWalkthroughCheck(MWCM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/deletecheck", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> deleteWalkthroughMasterCheckResources(
            @RequestParam("deleteRecid") String recid) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterWalkthroughCheckMod MWCM = new masterWalkthroughCheckMod();
                MWCM.setRecid(Integer.parseInt(recid));
                
                masterWalkthroughCheckDAO MWCDAO = new masterWalkthroughCheckDAO();
                String jsonResponse = MWCDAO.deleteMasterWalkthroughCheck(MWCM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
}
