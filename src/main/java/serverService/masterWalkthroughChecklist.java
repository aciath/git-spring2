/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverService;

import DAO.masterWalkthroughChecklistDAO;
import Model.masterWalkthroughChecklistMod;
import Model.responseInfoServices;
import java.sql.Connection;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @author user
 */
@RestController
@RequestMapping(value="/checklist")
public class masterWalkthroughChecklist {
    @Autowired
    ServletContext context;
    private Connection conn = null;
    
    // [GET] web server services untuk mengambil seluruh data master cabang dari mobile apps ke web services, mapping : http://ipserver:8080/cabang/getallcabang
    @CrossOrigin
    @RequestMapping(value="/getallchecklist", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public String getAllWalkthroughMasterChecklistResources() {
        String All = "";
        masterWalkthroughChecklistDAO MWCDAO = new masterWalkthroughChecklistDAO();
        All = MWCDAO.getAllMasterWalkthroughChecklist();
        return All;
    }
    
    @CrossOrigin
    @RequestMapping(value="/getspecificchecklist", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> getSpecificWalkthroughMasterChecklistResources(@Valid @RequestParam("recid") String recid) {
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterWalkthroughChecklistMod MWCM = new masterWalkthroughChecklistMod();
                MWCM.setRecid(Integer.parseInt(recid)); // ini adalah integer, bisa diubah sesuai dengan isi recid nanti di database
                
                masterWalkthroughChecklistDAO MWCDAO = new masterWalkthroughChecklistDAO();
                String jsonResponse = MWCDAO.getMasterWalkthroughChecklist(MWCM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/addchecklist", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> addWalkthroughMasterChecklistResources(
            @RequestParam("addChkObjectId") String chkObjectId,
            @RequestParam("addChkObjectUrut") String chkObjectUrut,
            @RequestParam("addChkObjectDetail") String chkObjectDetail,
            @RequestParam("addChkObjectDesc") String chkObjectDesc,
            @RequestParam("addChkObjectType") String chkObjectType,
            @RequestParam("addChkObjectNilai") String chkObjectNilai,
            @RequestParam("addIsChkObjectWt") String isChkObjectWt,
            @RequestParam("addIsChkObjectSa") String isChkObjectSa,
            @RequestParam("addIsChkObjectPict") String isChkObjectPict,
            @RequestParam("addCreatedOpt") String createdOpt) {
        
        // automatically add : created_opt, created_dt, edited_opt, edited_dt, client_address
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!chkObjectId.equals("")) {
            try {
                masterWalkthroughChecklistMod MWCM = new masterWalkthroughChecklistMod();
                MWCM.setChkObjectId(Integer.parseInt(chkObjectId));
                MWCM.setChkObjectUrut(Integer.parseInt(chkObjectUrut));
                MWCM.setChkObjectDetail(chkObjectDetail);
                MWCM.setChkObjectDesc(chkObjectDesc);
                MWCM.setChkObjectType(chkObjectType);
                MWCM.setChkObjectNilai(Integer.parseInt(chkObjectNilai));
                MWCM.setIschkObjectWt(isChkObjectWt);
                MWCM.setIschkObjectSa(isChkObjectSa);
                MWCM.setIschkObjectPict(isChkObjectPict);
                MWCM.setCreatedOpt(createdOpt);
                
                masterWalkthroughChecklistDAO MWCDAO = new masterWalkthroughChecklistDAO();
                String jsonResponse = MWCDAO.saveMasterWalkthroughChecklist(MWCM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/checklistobject", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> editWalkthroughMasterChecklistResources(
            @RequestParam("addChkObjectId") String chkObjectId,
            @RequestParam("addChkObjectUrut") String chkObjectUrut,
            @RequestParam("addChkObjectDetail") String chkObjectDetail,
            @RequestParam("addChkObjectDesc") String chkObjectDesc,
            @RequestParam("addChkObjectType") String chkObjectType,
            @RequestParam("addChkObjectNilai") String chkObjectNilai,
            @RequestParam("addIsChkObjectWt") String isChkObjectWt,
            @RequestParam("addIsChkObjectSa") String isChkObjectSa,
            @RequestParam("addIsChkObjectPict") String isChkObjectPict,
            @RequestParam("editEditedOpt") String editedOpt,
            @RequestParam("editRecid") String recid) {
        
        // automatically add : created_opt, created_dt, edited_opt, edited_dt, client_address
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("") && !chkObjectId.equals("")) {
            try {
                masterWalkthroughChecklistMod MWCM = new masterWalkthroughChecklistMod();
                MWCM.setChkObjectId(Integer.parseInt(chkObjectId));
                MWCM.setChkObjectUrut(Integer.parseInt(chkObjectUrut));
                MWCM.setChkObjectDetail(chkObjectDetail);
                MWCM.setChkObjectDesc(chkObjectDesc);
                MWCM.setChkObjectType(chkObjectType);
                MWCM.setChkObjectNilai(Integer.parseInt(chkObjectNilai));
                MWCM.setIschkObjectWt(isChkObjectWt);
                MWCM.setIschkObjectSa(isChkObjectSa);
                MWCM.setIschkObjectPict(isChkObjectPict);
                MWCM.setEditedOpt(editedOpt);
                MWCM.setRecid(Integer.parseInt(recid));
                
                masterWalkthroughChecklistDAO MWCDAO = new masterWalkthroughChecklistDAO();
                String jsonResponse = MWCDAO.updateMasterWalkthroughChecklist(MWCM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/deletechecklist", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> deleteWalkthroughMasterChecklistResources(
            @RequestParam("deleteRecid") String recid) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterWalkthroughChecklistMod MWCM = new masterWalkthroughChecklistMod();
                MWCM.setRecid(Integer.parseInt(recid));
                
                masterWalkthroughChecklistDAO MWCDAO = new masterWalkthroughChecklistDAO();
                String jsonResponse = MWCDAO.deleteMasterWalkthroughChecklist(MWCM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
}
