/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverService;

import DAO.masterWalkthroughObjectDAO;
import Model.masterWalkthroughObjectMod;
import Model.responseInfoServices;
import java.sql.Connection;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author user
 */
@RestController
@RequestMapping(value="/object")
public class masterWalkthroughObject {
    @Autowired
    ServletContext context;
    private Connection conn = null;
    
    // [GET] web server services untuk mengambil seluruh data master cabang dari mobile apps ke web services, mapping : http://ipserver:8080/cabang/getallcabang
    @CrossOrigin
    @RequestMapping(value="/getallobject", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public String getAllWalkthroughMasterObjectResources() {
        String All = "";
        masterWalkthroughObjectDAO MWODAO = new masterWalkthroughObjectDAO();
        All = MWODAO.getAllMasterWalkthroughObject();
        return All;
    }
    
    @CrossOrigin
    @RequestMapping(value="/getspecificobject", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> getSpecificWalkthroughMasterObjectResources(@Valid @RequestParam("recid") String recid) {
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterWalkthroughObjectMod MWOM = new masterWalkthroughObjectMod();
                MWOM.setRecid(Integer.parseInt(recid)); // ini adalah integer, bisa diubah sesuai dengan isi recid nanti di database
                
                masterWalkthroughObjectDAO MWODAO = new masterWalkthroughObjectDAO();
                String jsonResponse = MWODAO.getMasterWalkthroughObject(MWOM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/addobject", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> addWalkthroughMasterObjectResources(
            @RequestParam("addObjectBu") String objectBu,
            @RequestParam("addObjectDiv") String objectDiv,
            @RequestParam("addObjectActivities") String objectActivities,
            @RequestParam("addObjectWilayah") String objectWilayah,
            @RequestParam("addObjectLokasi") String objectLokasi,
            @RequestParam("addObjectName") String objectName,
            @RequestParam("addObjectUrut") String objectUrut,
            @RequestParam("addObjectPic") String objectPic,
            @RequestParam("addCreatedOpt") String createdOpt) {
        
        // automatically add : created_opt, created_dt, edited_opt, edited_dt, client_address
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!objectBu.equals("") && !objectDiv.equals("") && !objectActivities.equals("") && !objectWilayah.equals("") && !objectLokasi.equals("")) {
            try {
                masterWalkthroughObjectMod MWOM = new masterWalkthroughObjectMod();
                MWOM.setObjBu(objectBu);
                MWOM.setObjDiv(objectDiv);
                MWOM.setObjActivities(objectActivities);
                MWOM.setObjWilayah(objectWilayah);
                MWOM.setObjLokasi(objectLokasi);
                MWOM.setObjName(objectName);
                MWOM.setObjUrut(Integer.parseInt(objectUrut));
                MWOM.setObjPic(objectPic);
                MWOM.setCreatedOpt(createdOpt);
                
                masterWalkthroughObjectDAO MWODAO = new masterWalkthroughObjectDAO();
                String jsonResponse = MWODAO.saveMasterWalkthroughObject(MWOM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/editobject", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> editWalkthroughMasterObjectResources(
            @RequestParam("editObjectBu") String objectBu,
            @RequestParam("editObjectDiv") String objectDiv,
            @RequestParam("editObjectActivities") String objectActivities,
            @RequestParam("editObjectWilayah") String objectWilayah,
            @RequestParam("editObjectLokasi") String objectLokasi,
            @RequestParam("editObjectName") String objectName,
            @RequestParam("editObjectUrut") String objectUrut,
            @RequestParam("editObjectPic") String objectPic,
            @RequestParam("editEditedOpt") String editedOpt,
            @RequestParam("editRecid") int recid) {
        
        // automatically add : created_opt, created_dt, edited_opt, edited_dt, client_address
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!objectBu.equals("") && !objectDiv.equals("") && !objectActivities.equals("") && !objectWilayah.equals("") && !objectLokasi.equals("")) {
            try {
                masterWalkthroughObjectMod MWOM = new masterWalkthroughObjectMod();
                MWOM.setObjBu(objectBu);
                MWOM.setObjDiv(objectDiv);
                MWOM.setObjActivities(objectActivities);
                MWOM.setObjWilayah(objectWilayah);
                MWOM.setObjLokasi(objectLokasi);
                MWOM.setObjName(objectName);
                MWOM.setObjUrut(Integer.parseInt(objectUrut));
                MWOM.setObjPic(objectPic);
                MWOM.setEditedOpt(editedOpt);
                MWOM.setRecid(recid);
                
                masterWalkthroughObjectDAO MWODAO = new masterWalkthroughObjectDAO();
                String jsonResponse = MWODAO.updateMasterWalkthroughObject(MWOM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/deleteobject", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> deleteWalkthroughMasterActivitiesResources(
            @RequestParam("deleteRecid") String recid) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterWalkthroughObjectMod MWOM = new masterWalkthroughObjectMod();
                MWOM.setRecid(Integer.parseInt(recid));
                
                masterWalkthroughObjectDAO MWODAO = new masterWalkthroughObjectDAO();
                String jsonResponse = MWODAO.deleteMasterWalkthroughObject(MWOM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
}
