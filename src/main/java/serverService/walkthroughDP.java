/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverService;

import DAO.walkthroughDPController;
import Model.FileInfo;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author user
 */
@RestController
@RequestMapping(value="/imaging")
public class walkthroughDP {
    
    @Autowired
    ServletContext context;
    private Connection conn = null;
    
    // [GET] web server services untuk mengambil seluruh data scheduler dari mobile apps ke web services, mapping : http://ipserver:8080/downloadSchedulerList
    @CrossOrigin
    @RequestMapping(value="/downloadSchedulerList", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public String getAllWalkthruDPChecklistResources() {
        String All = "";
        walkthroughDPController WDPC = new walkthroughDPController();
        //All = WDPC.getWalkthroughList();
        return All;
    }
    
    // [POST] 
    @CrossOrigin
    @RequestMapping(value="/saveSchedulerList", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public String saveSchedulerListResources() {
        String All = "";
        walkthroughDPController WDPC = new walkthroughDPController();
        //
        return All;
    }
    
    // [PUT]
    @CrossOrigin
    @RequestMapping(value="/updateSchedulerList", method=RequestMethod.PUT, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public String updateSchedulerListResources() {
        String All = "";
        walkthroughDPController WDPC = new walkthroughDPController();
        //All = WDPC.getWalkthroughList();
        return All;
    }
    
    // [DELETE] 
    @CrossOrigin
    @RequestMapping(value="/removeSchedulerList", method=RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public String removeSchedulerListResources() {
        String All = "";
        walkthroughDPController WDPC = new walkthroughDPController();
        //All = WDPC.getWalkthroughList();
        return All;
    }
    
    // [GET] web server services untuk test koneksi ke web services, mapping : http://ipserver:8080/testService
    @CrossOrigin
    @RequestMapping(value="/testService", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public String testServices() throws JSONException {
        String All = "";
        JSONObject JSONObjectRoot = new JSONObject();
        JSONObjectRoot.put("Message", "Sarirasa Server Service Response Success");
        All += JSONObjectRoot.toString();
        return All;
    }
    
    // [POST] web server services untuk upload files, mapping : http://ipserver:8080/upload || setting upload files = application.properties
    @CrossOrigin
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<FileInfo> upload(@RequestParam("files") MultipartFile inputFile) {
        FileInfo fileInfo = new FileInfo();
        HttpHeaders headers = new HttpHeaders();
        if (!inputFile.isEmpty()) {
            try {
                String originalFilename = inputFile.getOriginalFilename();
                File destinationFile = new File(context.getRealPath("/uploaded/")+  File.separator + originalFilename);
                inputFile.transferTo(destinationFile);
                fileInfo.setFileName(destinationFile.getPath());
                fileInfo.setFileSize(inputFile.getSize());
                headers.add("File Uploaded Successfully - ", originalFilename);
                return new ResponseEntity<FileInfo>(fileInfo, headers, HttpStatus.OK);
            } catch (Exception e) { 
                System.out.println("file gagal di upload");
                e.printStackTrace();
                return new ResponseEntity<FileInfo>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("file gagal di upload / kosong");
            return new ResponseEntity<FileInfo>(HttpStatus.BAD_REQUEST);
        }
    }
    
//    @RequestMapping(value = "/upload", method =RequestMethod.POST)
//    public ResponseEntity<?> upload(@RequestParam("files") MultipartFile[] files) {
//        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
//        List<String> tempFileNames = new ArrayList<>();
//        String tempFileName;
//        FileOutputStream fo;
//
//        try {
//            for (MultipartFile file : files) {
//                tempFileName = "D:/uploaded/" + file.getOriginalFilename();
//                tempFileNames.add(tempFileName);
//                fo = new FileOutputStream(tempFileName);
//                fo.write(file.getBytes());
//                fo.close();
//                map.add("files", new FileSystemResource(tempFileName));
//            }
//
//            HttpHeaders headers = new HttpHeaders();
//            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
//
//            HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
//            //String response = restTemplate.postForObject(uploadFilesUrl, requestEntity, String.class);
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        for (String fileName : tempFileNames) {
//            File f = new File(fileName);
//            f.delete();
//        }
//        return new ResponseEntity<Object>(HttpStatus.OK);
//    }
}
