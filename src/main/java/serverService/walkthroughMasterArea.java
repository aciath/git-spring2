/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverService;

import DAO.masterWalkthroughAreaDAO;
import Model.masterWalkthroughAreaMod;
import Model.responseInfoServices;
import java.sql.Connection;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author user
 */
@RestController
@RequestMapping(value="/area")
public class walkthroughMasterArea {
    @Autowired
    ServletContext context;
    private Connection conn = null;
    
    // [GET] web server services untuk mengambil seluruh data master cabang dari mobile apps ke web services, mapping : http://ipserver:8080/cabang/getallcabang
    @CrossOrigin
    @RequestMapping(value="/getallarea", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public String getAllWalkthroughMasterAreaResources() {
        String All = "";
        masterWalkthroughAreaDAO MWADAO = new masterWalkthroughAreaDAO();
        All = MWADAO.getAllMasterWalkthroughArea();
        return All;
    }
    
    @CrossOrigin
    @RequestMapping(value="/getspecificarea", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> getSpecificWalkthroughMasterAreaResources(@Valid @RequestParam("areaid") String recid) {
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterWalkthroughAreaMod MWAM = new masterWalkthroughAreaMod();
                MWAM.setRecid(Integer.parseInt(recid)); // ini adalah integer, bisa diubah sesuai dengan isi recid nanti di database
                
                masterWalkthroughAreaDAO MWADAO = new masterWalkthroughAreaDAO();
                String jsonResponse = MWADAO.getAllMasterWalkthroughArea(MWAM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/addarea", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> addWalkthroughMasterAreaResources(
            @RequestParam("addAreaBU") String areaBu,
            @RequestParam("addAreaDiv") String areaDiv,
            @RequestParam("addAreaWilayah") String areaWilayah,
            @RequestParam("addAreaLokasi") String areaLokasi,
            @RequestParam("addIsfstand") String isfstand) {
        
        // automatically add : created_opt, created_dt, edited_opt, edited_dt, client_address
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!areaBu.equals("") && !areaDiv.equals("")) {
            try {
                masterWalkthroughAreaMod MWAM = new masterWalkthroughAreaMod();
                MWAM.setAreaBu(areaBu);
                MWAM.setAreaDiv(areaDiv);
                MWAM.setWilayah(areaWilayah);
                MWAM.setLokasi(areaLokasi);
                MWAM.setIsfstandOnly(isfstand);
                
                masterWalkthroughAreaDAO MWADAO = new masterWalkthroughAreaDAO();
                String jsonResponse = MWADAO.saveMasterWalkthroughArea(MWAM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/editarea", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> editWalkthroughMasterAreaResources(
            @RequestParam("recid") String recid,
            @RequestParam("editAreaBU") String areaBu,
            @RequestParam("editAreaDiv") String areaDiv,
            @RequestParam("editAreaWilayah") String areaWilayah,
            @RequestParam("editAreaLokasi") String areaLokasi,
            @RequestParam("editIsfstand") String isfstand) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterWalkthroughAreaMod MWAM = new masterWalkthroughAreaMod();
                MWAM.setRecid(Integer.parseInt(recid));
                MWAM.setAreaBu(areaBu);
                MWAM.setAreaDiv(areaDiv);
                MWAM.setWilayah(areaWilayah);
                MWAM.setLokasi(areaLokasi);
                MWAM.setIsfstandOnly(isfstand);
                
                masterWalkthroughAreaDAO MWADAO = new masterWalkthroughAreaDAO();
                String jsonResponse = MWADAO.updateMasterWalkthroughArea(MWAM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/deletearea", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> deleteWalkthroughMasterAreaResources(
            @RequestParam("recid") String recid ) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterWalkthroughAreaMod MWAM = new masterWalkthroughAreaMod();
                MWAM.setRecid(Integer.parseInt(recid));
                
                masterWalkthroughAreaDAO MWADAO = new masterWalkthroughAreaDAO();
                String jsonResponse = MWADAO.deleteMasterWalkthroughArea(MWAM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
}
