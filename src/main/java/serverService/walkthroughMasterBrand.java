/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverService;

import DAO.masterBrandDAO;
import DAO.walkthroughDPController;
import Model.FileInfo;
import Model.masterBrandMod;
import Model.responseInfoServices;
import java.io.File;
import java.sql.Connection;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author user
 */
@RestController
@RequestMapping(value="/brand")
public class walkthroughMasterBrand {
    
    @Autowired
    ServletContext context;
    private Connection conn = null;
    
    // [GET] web server services untuk mengambil seluruh data master brand dari mobile apps ke web services, mapping : http://ipserver:8080/downloadSchedulerList
    @CrossOrigin
    @RequestMapping(value="/getallbrand", method={RequestMethod.OPTIONS,RequestMethod.GET}, produces = { MediaType.APPLICATION_JSON_VALUE })
    public String getAllBrandResources() {
        String All = "";
        masterBrandDAO MWDAO = new masterBrandDAO();
        All = MWDAO.getAllmasterBrand();
        return All;
    }
    
    // [POST] web server services untuk mengambil spesifik data master brand dari mobile apps ke web services, mapping : http://ipserver:8080/downloadSchedulerList
    //@RequestMapping(value="/getSpecificBrand", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @RequestMapping(value="/getspecificbrand", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> getBrandResources(@Valid @RequestParam("brandId") String brandParam) {
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!brandParam.equals("")) {
            try {
                masterBrandMod MBM = new masterBrandMod();
                MBM.setBrandId(brandParam);
                MBM.setBrandName("");
                
                masterBrandDAO MWDAO = new masterBrandDAO();
                String jsonResponse = MWDAO.getMasterBrand(MBM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("brandParam is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    // [POST] web server services untuk mengambil add data master brand dari mobile apps ke web services, mapping : http://ipserver:8080/brand/addbrand
    @CrossOrigin
    @RequestMapping(value="/addbrand", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> addBrandResources(@RequestParam("brandId") String brandIdParam, @RequestParam("brandName") String brandNameParam) {
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!brandNameParam.equals("") && !brandIdParam.equals("")) {
            try {
                masterBrandMod MBM = new masterBrandMod();
                MBM.setBrandId(brandIdParam);
                MBM.setBrandName(brandNameParam);
                
                masterBrandDAO MWDAO = new masterBrandDAO();
                String jsonResponse = MWDAO.saveMasterBrand(MBM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("brandParam is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    // [POST] web server services untuk mengambil add data master brand dari mobile apps ke web services, mapping : http://ipserver:8080/brand/addbrand
    @CrossOrigin
    @RequestMapping(value="/updatebrand", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> updateBrandResources(@RequestParam("brandId") String brandIdParam, @RequestParam("brandName") String brandNameParam) {
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!brandNameParam.equals("") && !brandIdParam.equals("")) {
            try {
                masterBrandMod MBM = new masterBrandMod();
                MBM.setBrandId(brandIdParam);
                MBM.setBrandName(brandNameParam);
                
                masterBrandDAO MWDAO = new masterBrandDAO();
                String jsonResponse = MWDAO.updateMasterBrand(MBM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("brandParam is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    // [POST] web server services untuk mengambil add data master brand dari mobile apps ke web services, mapping : http://ipserver:8080/brand/addbrand
    @CrossOrigin
    @RequestMapping(value="/deletebrand", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> deleteBrandResources(@RequestParam("brandId") String brandIdParam) {
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!brandIdParam.equals("")) {
            try {
                masterBrandMod MBM = new masterBrandMod();
                MBM.setBrandId(brandIdParam);
                
                masterBrandDAO MWDAO = new masterBrandDAO();
                String jsonResponse = MWDAO.deleteMasterBrand(MBM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("brandParam is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
}
