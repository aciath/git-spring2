/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverService;

import DAO.masterCabangTypeDAO;
import Model.masterCabangTypeMod;
import Model.responseInfoServices;
import java.sql.Connection;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author user
 */
@RestController
@RequestMapping(value="/cabangtype")
public class walkthroughMasterCabangType {
    @Autowired
    ServletContext context;
    private Connection conn = null;
    
    // [GET] web server services untuk mengambil seluruh data master cabang dari mobile apps ke web services, mapping : http://ipserver:8080/cabang/getallcabang
    @CrossOrigin
    @RequestMapping(value="/getallcabangtype", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public String getAllCabangTypeResources() {
        String All = "";
        masterCabangTypeDAO MCTDAO = new masterCabangTypeDAO();
        All = MCTDAO.getAllMasterCabangType();
        return All;
    }
    
    // [GET] web server services untuk mengambil seluruh data master cabang dari mobile apps ke web services, mapping : http://ipserver:8080/cabang/getallcabang
    @CrossOrigin
    @RequestMapping(value="/getallbusinessunit", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public String getAllBusinessUnitResources() {
        String All = "";
        masterCabangTypeDAO MCTDAO = new masterCabangTypeDAO();
        All = MCTDAO.getAllMasterBusinessUnit();
        return All;
    }
    
    // [POST] web server services untuk mengambil spesifik data master cabang dari mobile apps ke web services, mapping : http://ipserver:8080/cabangtype/getspecificcabangtype
    //@RequestMapping(value="/getspecificcabang", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @CrossOrigin
    @RequestMapping(value="/getspecificcabangtype", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> getCabangTypeResources(@Valid @RequestParam("typeId") String typeId) {
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!typeId.equals("")) {
            try {
                masterCabangTypeMod MCTM = new masterCabangTypeMod();
                MCTM.setTypeId(typeId);
                
                masterCabangTypeDAO MCTDAO = new masterCabangTypeDAO();
                String jsonResponse = MCTDAO.getMasterCabangType(MCTM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    // [POST] web server services untuk menambahkan data master cabang ke web services, mapping : http://ipserver:8080/cabang/addcabang
    @CrossOrigin
    @RequestMapping(value="/addcabangtype", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> addCabangTypeResources(
            @RequestParam("typeId") String typeId, 
            @RequestParam("typeName") String typeName,
            @RequestParam("typeBu") String typeBu) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!typeId.equals("")) {
            try {
                masterCabangTypeMod MCTM = new masterCabangTypeMod();
                MCTM.setTypeId(typeId);
                MCTM.setTypeName(typeName);
                MCTM.setTypeBu(typeBu);
                
                masterCabangTypeDAO MCTDAO = new masterCabangTypeDAO();
                String jsonResponse = MCTDAO.saveMasterCabangType(MCTM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    // [PUT] web server services untuk mengubah data master cabang ke web services, mapping : http://ipserver:8080/cabang/addcabang
    @CrossOrigin
    @RequestMapping(value="/editcabangtype", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> editCabangTypeResources(
            @RequestParam("typeId") String typeId, 
            @RequestParam("typeName") String typeName,
            @RequestParam("typeBu") String typeBu) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!typeId.equals("")) {
            try {
                masterCabangTypeMod MCTM = new masterCabangTypeMod();
                MCTM.setTypeId(typeId);
                MCTM.setTypeName(typeName);
                MCTM.setTypeBu(typeBu);
                
                masterCabangTypeDAO MCTDAO = new masterCabangTypeDAO();
                String jsonResponse = MCTDAO.updateMasterCabangType(MCTM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    // [DELETE] web server services untuk menghapus data master cabang ke web services, mapping : http://ipserver:8080/cabang/deletecabang?cabangId=?
    @CrossOrigin
    @RequestMapping(value="/deletecabangtype", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> deleteCabangTypeResources(@RequestParam("typeId") String typeId) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!typeId.equals("")) {
            try {
                masterCabangTypeMod MCTM = new masterCabangTypeMod();
                MCTM.setTypeId(typeId);
                
                masterCabangTypeDAO MCTDAO = new masterCabangTypeDAO();
                String jsonResponse = MCTDAO.deleteMasterCabangType(MCTM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
}
