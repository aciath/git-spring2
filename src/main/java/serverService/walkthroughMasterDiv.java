/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverService;

import DAO.masterWalkthroughDivDAO;
import Model.masterWalkthroughDivMod;
import Model.responseInfoServices;
import java.sql.Connection;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author user
 */
@RestController
@RequestMapping(value="/div")
public class walkthroughMasterDiv {
    @Autowired
    ServletContext context;
    private Connection conn = null;
    
    // [GET] web server services untuk mengambil seluruh data master cabang dari mobile apps ke web services, mapping : http://ipserver:8080/cabang/getallcabang
    @CrossOrigin
    @RequestMapping(value="/getalldiv", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public String getAllDivResources() {
        String All = "";
        masterWalkthroughDivDAO MWDDAO = new masterWalkthroughDivDAO();
        All = MWDDAO.getAllMasterWalkthroughDiv();
        return All;
    }
    
    // [POST] web server services untuk mengambil spesifik data master cabang dari mobile apps ke web services, mapping : http://ipserver:8080/cabangtype/getspecificcabangtype
    //@RequestMapping(value="/getspecificcabang", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE})
    @CrossOrigin
    @RequestMapping(value="/getspecificdiv", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> getDivResources(@Valid @RequestParam("divId") String divId) {
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!divId.equals("")) {
            try {
                masterWalkthroughDivMod MWDM = new masterWalkthroughDivMod();
                MWDM.setDivId(divId);
                
                masterWalkthroughDivDAO MWDDAO = new masterWalkthroughDivDAO();
                String jsonResponse = MWDDAO.getMasterWalkthroughDiv(MWDM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    // [POST] web server services untuk menambahkan data master cabang ke web services, mapping : http://ipserver:8080/cabang/addcabang
    @CrossOrigin
    @RequestMapping(value="/adddiv", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> addDivResources(
            @RequestParam("divId") String divId, 
            @RequestParam("divDescription") String divDescription,
            @RequestParam("createdOpt") String createdOpt) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!divId.equals("")) {
            try {
                masterWalkthroughDivMod MWDM = new masterWalkthroughDivMod();
                MWDM.setDivId(divId);
                MWDM.setDivDescription(divDescription);
                MWDM.setCreatedOpt(createdOpt);
                
                masterWalkthroughDivDAO MWDDAO = new masterWalkthroughDivDAO();
                String jsonResponse = MWDDAO.saveMasterWalkthroughDiv(MWDM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    // [PUT] web server services untuk mengubah data master cabang ke web services, mapping : http://ipserver:8080/cabang/addcabang
    @CrossOrigin
    @RequestMapping(value="/editdiv", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> editDivResources(
            @RequestParam("divId") String divId, 
            @RequestParam("divDescription") String divDescription) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!divId.equals("")) {
            try {
                masterWalkthroughDivMod MWDM = new masterWalkthroughDivMod();
                MWDM.setDivId(divId);
                MWDM.setDivDescription(divDescription);
                
                masterWalkthroughDivDAO MWDDAO = new masterWalkthroughDivDAO();
                String jsonResponse = MWDDAO.updateMasterWalkthroughDiv(MWDM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    // [DELETE] web server services untuk menghapus data master cabang ke web services, mapping : http://ipserver:8080/cabang/deletecabang?cabangId=?
    @CrossOrigin
    @RequestMapping(value="/deletediv", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> deleteDivResources(@RequestParam("divId") String divId) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!divId.equals("")) {
            try {
                masterWalkthroughDivMod MWDM = new masterWalkthroughDivMod();
                MWDM.setDivId(divId);
                
                masterWalkthroughDivDAO MWDDAO = new masterWalkthroughDivDAO();
                String jsonResponse = MWDDAO.deleteMasterCabangType(MWDM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
}
