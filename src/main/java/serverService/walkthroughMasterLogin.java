/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverService;

import DAO.masterWalkthroughLoginDAO;
import Model.masterWalkthroughLoginMod;
import Model.responseInfoServices;
import java.sql.Connection;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author user
 */
@RestController
@RequestMapping(value="/login")
public class walkthroughMasterLogin {
    @Autowired
    ServletContext context;
    private Connection conn = null;
    
    // [GET] web server services untuk mengambil seluruh data master cabang dari mobile apps ke web services, mapping : http://ipserver:8080/cabang/getallcabang
    @CrossOrigin
    @RequestMapping(value="/getalllogin", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public String getAllWalkthroughMasterLoginResources() {
        String All = "";
        masterWalkthroughLoginDAO MWLDAO = new masterWalkthroughLoginDAO();
        All = MWLDAO.getAllMasterWalkthroughLogin();
        return All;
    }
    
    @CrossOrigin
    @RequestMapping(value="/getspecificlogin", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> getSpecificWalkthroughMasterLoginResources(@Valid @RequestParam("recid") String recid) {
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterWalkthroughLoginMod MWLM = new masterWalkthroughLoginMod();
                MWLM.setRecid(Integer.parseInt(recid)); // ini adalah integer, bisa diubah sesuai dengan isi recid nanti di database
                
                masterWalkthroughLoginDAO MWLDAO = new masterWalkthroughLoginDAO();
                String jsonResponse = MWLDAO.getMasterWalkthroughLogin(MWLM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/addlogin", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> addWalkthroughMasterLoginResources(
            @RequestParam("addLoginName") String loginName,
            @RequestParam("addDeviceId") String deviceId,
            @RequestParam("addCreatedOpt") String createdOpt) {
        
        // automatically add : created_opt, created_dt, edited_opt, edited_dt, client_address
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!loginName.equals("")) {
            try {
                masterWalkthroughLoginMod MWLM = new masterWalkthroughLoginMod();
                MWLM.setLoginName(loginName);
                MWLM.setDeviceId(Integer.parseInt(deviceId));
                MWLM.setCreatedOpt(createdOpt);
                
                masterWalkthroughLoginDAO MWLDAO = new masterWalkthroughLoginDAO();
                String jsonResponse = MWLDAO.saveMasterWalkthroughLogin(MWLM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/editlogin", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> editWalkthroughMasterLoginResources(
            @RequestParam("editLoginName") String loginName,
            @RequestParam("editDeviceId") String deviceId,
            @RequestParam("editEditedOpt") String editedOpt,
            @RequestParam("editRecid") String recid) {
        
        // automatically add : created_opt, created_dt, edited_opt, edited_dt, client_address
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!loginName.equals("")) {
            try {
                masterWalkthroughLoginMod MWLM = new masterWalkthroughLoginMod();
                MWLM.setLoginName(loginName);
                MWLM.setDeviceId(Integer.parseInt(deviceId));
                MWLM.setEditedOpt(editedOpt);
                MWLM.setRecid(Integer.parseInt(recid));
                
                masterWalkthroughLoginDAO MWLDAO = new masterWalkthroughLoginDAO();
                String jsonResponse = MWLDAO.updateMasterWalkthroughLogin(MWLM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/deletelogin", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> deleteWalkthroughMasterAreaResources(
            @RequestParam("recid") String recid ) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterWalkthroughLoginMod MWLM = new masterWalkthroughLoginMod();
                MWLM.setRecid(Integer.parseInt(recid));
                
                masterWalkthroughLoginDAO MWLDAO = new masterWalkthroughLoginDAO();
                String jsonResponse = MWLDAO.deleteMasterWalkthroughLogin(MWLM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
}
