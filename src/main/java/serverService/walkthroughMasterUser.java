/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverService;


import DAO.masterWalkthroughUserDAO;
import Model.masterWalkthroughUserMod;
import Model.responseInfoServices;
import java.sql.Connection;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author user
 */
@RestController
@RequestMapping(value="/user")
public class walkthroughMasterUser {
    @Autowired
    ServletContext context;
    private Connection conn = null;
    
    // [GET] web server services untuk mengambil seluruh data master cabang dari mobile apps ke web services, mapping : http://ipserver:8080/cabang/getallcabang
    @CrossOrigin
    @RequestMapping(value="/getalluser", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public String getAllWalkthroughMasterUserResources() {
        String All = "";
        masterWalkthroughUserDAO MWUDAO = new masterWalkthroughUserDAO();
        All = MWUDAO.getAllMasterWalkthroughUser();
        return All;
    }
    
    @CrossOrigin
    @RequestMapping(value="/getspecificuser", method=RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> getSpecificWalkthroughMasterUserResources(@Valid @RequestParam("userid") String userid) {
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!userid.equals("")) {
            try {
                masterWalkthroughUserMod MWUM = new masterWalkthroughUserMod();
                MWUM.setUserId(userid); // ini adalah integer, bisa diubah sesuai dengan isi recid nanti di database
                
                masterWalkthroughUserDAO MWUDAO = new masterWalkthroughUserDAO();
                String jsonResponse = MWUDAO.getMasterCabangType(MWUM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/adduser", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> addWalkthroughMasterUserResources(
            @RequestParam("addUserId") String userId,
            @RequestParam("addUserDiv") String userDiv,
            @RequestParam("addUserArea") String userArea,
            @RequestParam("addIsAllowWt") String isallowWt,
            @RequestParam("addIsAllowSa") String isallowSa,
            @RequestParam("addCreatedOpt") String createdOpt) {
        
        // automatically add : created_opt, created_dt, edited_opt, edited_dt, client_address
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!userId.equals("")) {
            try {
                masterWalkthroughUserMod MWUM = new masterWalkthroughUserMod();
                MWUM.setUserId(userId);
                MWUM.setUserDiv(userDiv);
                MWUM.setUserArea(userArea);
                MWUM.setIsallowWt(isallowWt);
                MWUM.setIsallowSa(isallowSa);
                MWUM.setCreatedOpt(createdOpt);
                
                masterWalkthroughUserDAO MWUDAO = new masterWalkthroughUserDAO();
                String jsonResponse = MWUDAO.saveMasterWalkthroughUser(MWUM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/edituser", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> editWalkthroughMasterAreaResources(
            @RequestParam("recid") String recid,
            @RequestParam("editUserId") String userId,
            @RequestParam("editUserDiv") String userDiv,
            @RequestParam("editUserArea") String userArea,
            @RequestParam("editIsAllowWt") String isallowWt,
            @RequestParam("editIsAllowSa") String isallowSa,
            @RequestParam("editEditedOpt") String editedOpt) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterWalkthroughUserMod MWUM = new masterWalkthroughUserMod();
                MWUM.setRecid(Integer.parseInt(recid));
                MWUM.setUserId(userId);
                MWUM.setUserDiv(userDiv);
                MWUM.setUserArea(userArea);
                MWUM.setIsallowWt(isallowWt);
                MWUM.setIsallowSa(isallowSa);
                MWUM.setEditedOpt(editedOpt);
                
                masterWalkthroughUserDAO MWUDAO = new masterWalkthroughUserDAO();
                String jsonResponse = MWUDAO.updateMasterWalkthroughUser(MWUM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @CrossOrigin
    @RequestMapping(value="/deleteuser", method=RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<responseInfoServices> deleteWalkthroughMasterAreaResources(
            @RequestParam("recid") String recid ) {
        
        responseInfoServices RIS = new responseInfoServices();
        HttpHeaders headers = new HttpHeaders();
        
        if(!recid.equals("")) {
            try {
                masterWalkthroughUserMod MWUM = new masterWalkthroughUserMod();
                MWUM.setRecid(Integer.parseInt(recid));
                
                masterWalkthroughUserDAO MWUDAO = new masterWalkthroughUserDAO();
                String jsonResponse = MWUDAO.deleteMasterWalkthroughUser(MWUM);

                RIS.setJsonResponse(jsonResponse);
                
                headers.add("Response", jsonResponse);
                return new ResponseEntity<responseInfoServices>(RIS, headers, HttpStatus.OK);
            } catch (Exception e) { 
                e.printStackTrace();
                return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
            }
        } else {
            System.out.println("parameters is null");
            return new ResponseEntity<responseInfoServices>(HttpStatus.BAD_REQUEST);
        }
    }
}
